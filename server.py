from http.server import *
import secrets
import json
import auth
import createDB
import Utils
import Top_Badges
import topics
import backup


class SimpleHandler(SimpleHTTPRequestHandler):
    path: str

    def do_GET(self):
        if self.path is "/":
            Utils.send_static_files(self, "/entryPage.html")
        elif self.path == "/categoriesGet":
            topics.getCategories(self)
        elif self.path.startswith("/goalsGet?"):
            headers = [("Content-Type", "text/plain")]
            query_param = Utils.get_query_strings(self.path)
            if "authToken" in query_param:
                authToken = query_param["authToken"][0].replace("\"", "")
                userId = createDB.loggedUsers_GetId(authToken)
                if userId:
                    Utils.send_response(self, 200, headers, json.dumps(Top_Badges.goals_read(userId)))
        elif self.path == "/topGet":
            headers = [("Content-Type", "text/plain")]
            Utils.send_response(self, 200, headers, json.dumps(Top_Badges.top_read()))
        elif self.path.startswith("/validateUser?"):
            # validate user token
            query_param = Utils.get_query_strings(self.path)
            if "authToken" in query_param:
                auth_token = query_param["authToken"][0].replace("\"", "")
                if createDB.verify_auth_token(auth_token):
                    print('Valid token!')
                    Utils.send_response(self, 200, [("Content-Type", "text/plain")], json.dumps({"Status": True}))
                else:
                    print('Invalid token')
                    self.send_response(401)
                    response = {'Status': False}
                    self.send_header("Content-Type", 'text/plain')
                    self.end_headers()
                    self.wfile.write(bytes(json.dumps(response), 'utf-8'))
        elif self.path.startswith("/topics?"):
            topics.getTopics(self)
        elif self.path.startswith("/slides?"):
            topics.getSlides(self)
        elif self.path.startswith("/quiz?"):
            topics.getQuiz(self)
        elif self.path.startswith("/access_level?"):
            topics.getAccessLevels(self)
        elif self.path.startswith("/topPlayers.html?"):
            query_param = Utils.get_query_strings(self.path)
            if "rss" in query_param:
                rss = query_param["rss"][0]
                with open('views/rssFeed/rssFeed.xml', 'w') as f:
                    f.write(rss)
                    f.close()
                self.path = 'rssFeed.xml'
                Utils.send_static_files(self, self.path)
        elif len(self.path.split(".")) > 0:
            Utils.send_static_files(self, self.path)
        else:
            Utils.send_response(self, 404, [("Content-Type", "text/plain")], json.dumps({"Error": "Resource Not Found"}))


    def do_POST(self):
        response = {}
        if self.path == '/login.html':
            print(self.path)
            print("Receiving login information from user ...")
            try:
                post_data = self.rfile.read(int(self.headers['Content-Length']))
                post_data_dict = json.loads(str(post_data, encoding='utf-8'))
                email = post_data_dict['email']
                password = post_data_dict['password']
                # get user id
                user_id = createDB.get_user_id(email)
                # verifies if the login is successful and updates token in db
                if createDB.get_auth_token(user_id) and auth.login(email, password):
                    print('Successfully login')
                    # updates token in db
                    encoded_auth_token = secrets.token_hex(16)
                    createDB.update_auth_token(user_id, encoded_auth_token)
                    response = {'status': True, 'authToken': encoded_auth_token, 'email': email}
                    self.send_response(200)
                else:  # USER NOT RECOGNIZED, MESSAGE
                    print('User not recognized')
                    response = {'status': False}
                    self.send_response(401)  # unauthorized
            except json.decoder.JSONDecodeError:
                response = {'status': False}
            finally:
                self.send_header("Content-Type", 'text/plain')
                self.end_headers()
                self.wfile.write(bytes(json.dumps(response), 'utf-8'))
        if self.path == '/createAccount.html':
            try:
                post_data = self.rfile.read(int(self.headers['Content-Length']))
                post_data_dict = json.loads(str(post_data, encoding='utf-8'))
                email = post_data_dict['email']
                password = post_data_dict['password']
                password2 = post_data_dict['password2']
                if password == password2:
                    if auth.create_account(email, password):
                        print('Account created!')
                        encoded_auth_token = secrets.token_hex(16)
                        # get user id
                        user_id = createDB.get_user_id(email)
                        # create auth table if it doesn't exist
                        createDB.create_auth_table()
                        # create user entry in auth_table DB (id + token)
                        createDB.add_auth_user(user_id, encoded_auth_token)
                        # create access level for all categories
                        createDB.newUser_createAccessLevel(user_id)
                        # send token to FE
                        response = {'status': True, 'authToken': encoded_auth_token, 'email': email}
                        self.send_response(200)
                    else:
                        print("Email already taken. Please try again!")
                        response = {'status': False}
                        self.send_response(409)
                else:
                    print('Passwords do not match. Try again!')
                    response = {'status': False}
                    self.send_response(401)  # unauthorized
            except json.decoder.JSONDecodeError:
                response = {'status': False}
            finally:
                self.send_header("Content-Type", 'text/plain')
                self.end_headers()
                self.wfile.write(bytes(json.dumps(response), 'utf-8'))
        elif self.path == "/quizCheck":
            topics.quizCheck(self)

            # answers = post_data_dict["answers"]
            # print(type(answers[0]))


# Start the database backups making thread
backup.start_backups_thread()


PORT = 4000
server = HTTPServer(("", PORT), SimpleHandler)
print("Serving at port: ", PORT)
server.serve_forever()
