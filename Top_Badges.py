import sqlite3
import createDB
import math
# CRUD(Create, Read, Update, Delete) functions


def goals_read(authUserId):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = 'SELECT * FROM badges'
    cursor.execute(command)
    goals = []
    for i in cursor.fetchall():
        goals.append({"id": i[0], "name": i[1]})

    command = 'SELECT * FROM users_badges'
    # command = 'SELECT * FROM users_badges WHERE user_id = ' + str(1)
    cursor.execute(command)
    users_badges = []
    for i in cursor.fetchall():
        users_badges.append({"id": i[0], "user_id": i[1], "badge_id": i[2]})

    htmlBadges = []
    for goal in goals:
        # Check if user did it
        isDone = False
        for j in users_badges:
            if j["user_id"] == authUserId and j["badge_id"] == goal["id"]:
                isDone = True
        htmlBadges.append({"id": goal["id"], "name": goal["name"], "isDone": isDone})
    return htmlBadges


def truncate(number, digits) -> float:
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper


def top_read():
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = 'SELECT * FROM users'
    cursor.execute(command)
    users = []
    for i in cursor.fetchall():
        users.append({"id": i[0], "username": i[2], "top": 0, "progress": 0, "score": 0, "badges": 0, "gender": True})

    command = 'SELECT * FROM quiz_scores'
    cursor.execute(command)
    scores = []
    for i in cursor.fetchall():
        scores.append({"id": i[0], "user_id": i[1], "score": i[4]})

    for score in scores:
        for user in users:
            if score["user_id"] == user["id"]:
                user["score"] += score["score"]

    command = 'SELECT * FROM users_badges'
    cursor.execute(command)
    users_badges = []
    for i in cursor.fetchall():
        users_badges.append({"id": i[0], "user_id": i[1], "badge_id": i[2]})

    for user_badge in users_badges:
        for user in users:
            if user_badge["user_id"] == user["id"]:
                user["badges"] += 1

#     Sort users
    for i in range(0, len(users)-1):
        for j in range(i, len(users)):
            if users[i]["score"] < users[j]["score"]:
                aux = users[i]
                users[i] = users[j]
                users[j] = aux

    command = 'SELECT * FROM topics'
    cursor.execute(command)
    maxScore = len(cursor.fetchall()) * 3

    cnt = 1
    for user in users:
        user["top"] = cnt
        cnt += 1
        if user["top"] % 2 == 0:
            user["gender"] = False
        user["progress"] = truncate(user["score"] * 100 / maxScore, 2)

    return users[:10]






# Badges
# createDB.add_badge("First Log")
# createDB.add_badge("Character Customization")
# createDB.add_badge("Your Name")

# Users badges
# createDB.add_badge_to_user(1, 1)
# createDB.add_badge_to_user(6, 1)
