import json
from urllib.parse import urlparse
from urllib.parse import parse_qs


#
#    Function for sending a response
#    Example:
#    headers = [("Content-Type", "text/plain")]
#    Utils.send_response(request, 200, headers, json.dumps(content_to_send))
#
def send_response(response, status_code, headers, contents):
    response.send_response(status_code)
    for i in headers:
        response.send_header(i[0], i[1])
    response.end_headers()
    if type(contents) == type(b'abc'):
        response.wfile.write(contents)
    else:
        response.wfile.write(bytes(contents, encoding='utf-8'))


def set_headers(extension):
    if extension == "html":
        return [("Content-Type", "text/html")]
    elif extension == "css":
        return [("Content-Type", "text/css")]
    elif extension == "jpeg" or extension == "jpg":
        return [("Content-Type", "image/jpeg")]
    elif extension == "js":
        return [("Content-Type", "text/js")]
    elif extension == "png":
        return [("Content-Type", "image/png")]
    elif extension == "ico":
        return [("Content-Type", "image/vnd.microsoft.icon")]
    elif extension == "xml":
        return [("Content-Type", "text/xml")]
    else:
        return []


#
#   Function for sending static files(.html, .css, .js, images)
#
def send_static_files(response, path):
    try:
        path_split = path.split(".")
        if path_split[1] == "html" or path_split[1] == "css" \
                or path_split[1] == "js" or path_split[1] == "xml":
            file_path = "views" + path_split[0] + path
            headers = set_headers(path_split[1])
            file_to_open = open(file_path).read()
            send_response(response, 200, headers, file_to_open)
        else:
            file_path = "views/images" + path
            headers = set_headers(path_split[1])
            file_to_open = open(file_path, "rb").read()
            send_response(response, 200, headers, file_to_open)
    except:
        headers = [("Content-Type", "text/plain")]
        send_response(response, 500, headers, json.dumps({"error": "Internal server error"}))


#
#   Function for parsing the request body
#   request: self
#   Return a dict with the request body key-value pairs
#
def parse_request_body(request):
    post_data = request.rfile.read(int(request.headers['Content-Length']))
    post_data_dict = json.loads(str(post_data, encoding='utf-8'))
    return post_data_dict


def is_integer(n):
    try:
        float(n)
    except ValueError:
        return False
    else:
        return float(n).is_integer()


#
#   Function for parsing query string
#   path: self.path
#   Returns a dict with the query string key-value pairs
#   Use only when you know you have query strings
#
def get_query_strings(path):
    parsed_url = urlparse(path)
    query = parse_qs(parsed_url.query)
    return query
