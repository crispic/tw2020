import sqlite3
import hashlib


def login(email, password):
    while True:
        with sqlite3.connect("GaL.db") as db:
            cursor = db.cursor()
        get_user = "SELECT * FROM users WHERE email =:email AND password =:password"
        hash_password = hashlib.md5(password.encode()).hexdigest()
        cursor.execute(get_user, {'email': email, 'password': hash_password})
        results = cursor.fetchall()
        if results:
            return True
        else:
            return False


def create_account(email, password):
    found = 0
    while found == 0:
        with sqlite3.connect("GaL.db") as db:
            cursor = db.cursor()
        get_user = "SELECT * FROM users WHERE email =:email"
        cursor.execute(get_user, {'email': email})
        if cursor.fetchall():  # email already taken
            return False
        else:
            found = 1
    if found == 1:
        username = email.split('@')[0]
        create_user = ''' INSERT INTO users(email, username, password) VALUES(:email, :username, :password)'''
        hash_password = hashlib.md5(password.encode()).hexdigest()
        cursor.execute(create_user, {'email': email, 'username': username, 'password': hash_password})
        db.commit()
        return True

