import json
import Utils
import createDB


def getTopics(request):
    query_param = Utils.get_query_strings(request.path)
    if "catId" in query_param:
        if Utils.is_integer(query_param["catId"][0]) and int(query_param["catId"][0]) != 0:
            Utils.send_response(request,
                                200,
                                [("Content-Type", "text/plain")],
                                json.dumps(createDB.topics_read(int(query_param["catId"][0]))))
        else:
            Utils.send_response(request, 404, [("Content-Type", "text/plain")],
                                json.dumps({"Error": "Category not found"}))
    else:
        Utils.send_response(request, 400, [("Content-Type", "text/plain")],
                            json.dumps({"Error": "Bad Request"}))


def getSlides(request):
    query_param = Utils.get_query_strings(request.path)
    if "catId" in query_param and "topicId" in query_param:
        if (Utils.is_integer(query_param["catId"][0]) and int(query_param["catId"][0]) != 0) and (
                Utils.is_integer(query_param["topicId"][0]) and int(query_param["topicId"][0]) != 0):
            Utils.send_response(request, 200, [("Content-Type", "text/plain")],
                                json.dumps(createDB.slides_read(int(query_param["catId"][0]),
                                                            int(query_param["topicId"][0]))))
        else:
            Utils.send_response(request, 404, [("Content-Type", "text/plain")],
                                json.dumps({"Error": "Slides not found"}))
    else:
        Utils.send_response(request, 400, [("Content-Type", "text/plain")],
                            json.dumps({"Error": "Bad Request"}))


def getQuiz(request):
    query_param = Utils.get_query_strings(request.path)
    if "catId" in query_param and "topicId" in query_param:
        if (Utils.is_integer(query_param["catId"][0]) and int(query_param["catId"][0]) != 0) and (
                Utils.is_integer(query_param["topicId"][0]) and int(query_param["topicId"][0]) != 0):
            Utils.send_response(request, 200, [("Content-Type", "text/plain")],
                                json.dumps(createDB.quiz_read(int(query_param["catId"][0]),
                                                          int(query_param["topicId"][0]))))
        else:
            Utils.send_response(request, 404, [("Content-Type", "text/plain")],
                                json.dumps({"Error": "Quiz not found"}))
    else:
        Utils.send_response(request, 400, [("Content-Type", "text/plain")],
                            json.dumps({"Error": "Bad Request"}))


def getAccessLevels(request):
    query_param = Utils.get_query_strings(request.path)
    if "catId" in query_param and "authToken" in query_param:
        authToken = query_param["authToken"][0].replace("\"", "")
        catId = int(query_param["catId"][0])
        userId = createDB.loggedUsers_GetId(authToken)
        if catId != 0 and userId:
            Utils.send_response(request, 200, [("Content-Type", "text/plain")], json.dumps(
                {"access_level": createDB.access_level_get(int(query_param["catId"][0]), userId)}))


def getCategories(request):
    headers = [("Content-Type", "text/plain")]
    Utils.send_response(request, 200, headers, json.dumps(createDB.categories_read()))


def quizCheck(request):
    post_data_dict = Utils.parse_request_body(request)
    token = post_data_dict["authToken"].replace("\"", "")
    userId = createDB.loggedUsers_GetId(token)
    if userId:
        answers = post_data_dict["answers"]
        score = 0
        for i in answers:
            question = i
            correctAnswer = createDB.quiz_getRightAnswer(int(question["id"]))
            if correctAnswer == question["answer"]:
                score += 1
        if score == len(answers):
            createDB.quiz_score_create({"userId": userId, "categoryId": int(answers[0]["catId"]),
                                    "topicId": int(answers[0]["topicId"]), "score": score})
            createDB.access_level_update(int(answers[0]["catId"]), userId, int(answers[0]["topicId"]) + 1)
            # Add to users_badges new entry
            createDB.add_badge_to_user(userId, int(answers[0]["topicId"]))
            Utils.send_response(request, 200, [("Content-Type", "text/plain")],
                                json.dumps({"status": True, "message": ""}))
        else:
            Utils.send_response(request, 200, [("Content-Type", "text/plain")],
                                json.dumps({"status": False, "message": ""}))
    else:
        Utils.send_response(request, 200, [("Content-Type", "text/plain")],
                            json.dumps({"status": False, "message": "not auth"}))
