import threading
import shutil
from datetime import datetime
import os
import time


def thread_func():
    while True:
        now = datetime.now()
        current_time = time.time()
        if now.minute == 0 and now.second == 0:
            print('Creating a backup of the database')
            createBackup()
            time.sleep(1)
        # Remove files that were created >= 7 days ago
        if now.hour == 0 and now.minute == 0 and now.second == 0:
            for f in os.listdir(os.path.dirname(os.path.abspath('GaL.db')) + '\\backups\\'):
                creation_time = os.path.getctime(f)
                if (current_time - creation_time) // (24 * 3600) >= 7:
                    os.unlink(f)
                    print('{} removed'.format(f))
            time.sleep(1)


def createBackup():
    now = datetime.now()
    date_string = now.strftime("%d %m %Y %H %M %S")
    original = os.path.abspath('GaL.db')
    target = os.path.dirname(os.path.abspath('GaL.db'))
    target += '\\backups\\'
    target += date_string
    target += '.db'
    print('Created a backup of database at:')
    print(target)
    shutil.copy(original, target)


x = threading.Thread(target=thread_func)


def start_backups_thread():
    try:
        x.start()
    except:
        print('Unable to start backups thread')
