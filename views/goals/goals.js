function moveProgressBar(end) {
  var width = 0;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= end) {
      clearInterval(id);
    } else {
      width++;
      document.documentElement.style.setProperty("--progress", width.toString() + '%');
    }
  }
}

function extendNavbar() {
  var x = document.getElementById("myTopnav");
  if (x.className == "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}


function generateGoalsHtml(goals){
  donebadges = 0 
  var a = "";

  // Adding the progress bar
  a += "<div class=\"progressBarContainer\"> \
        <div class=\"text\">Your progress</div> \
        <div id=\"progressBar\"></div> \
        </div>\n"

  for(i=0;i<goals.length;i++){
      var content = ""
      content += "<div class=\"badge\">"
      if (goals[i].isDone == true)
        {
          content += "<img alt=\"done\" src=\"/Done.png\" />"
          donebadges += 1
        }
      else
        content += "<img alt=\"notDone\" src=\"/notDone.png\" />"
      content += "<div class=\"text\">" + goals[i].name + "</div>"
      content += "</div>"
      a += content
  }

  end = donebadges * 100 / goals.length;
  moveProgressBar(end);

  return a;
}

function setGoals(){
  getPage('/goals.html');
  authToken = getFromStorage("session", "authToken").value
  var request = new XMLHttpRequest()
  var url = "/goalsGet?authToken=" + authToken
  request.open("GET", url)
  request.send()
  request.onreadystatechange=(e)=>{
      if(request.readyState === 4 && request.status === 200){
          var resp = JSON.parse(request.responseText)
          document.getElementById("goalsContainer").innerHTML = generateGoalsHtml(resp);
      }
  }
}
