window.onload = () => {
    var authToken = sessionStorage.getItem('authToken');
    var xhr = new XMLHttpRequest();
    xhr.open( "GET", '/createAccount.html', true ); // false for synchronous request
    xhr.send( null );
    xhr.onload = (e) => {
    if(xhr.readyState === 4 && xhr.status === 200) {
        var resp = xhr.responseText;
        return resp;
    } else if (xhr.status === 404) {
        console.log('File not found');
    }
  }
}

function createAccount(){
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    let password2 = document.getElementById('password2').value;
    if(email && password && password2){
         post_data = {
           'email': email,
           'password': password,
           'password2': password2
         }
         let xhr = new XMLHttpRequest();
         xhr.open('POST', '/createAccount.html', true);
         xhr.send(JSON.stringify(post_data));
         xhr.onload = (e) => {
            let messageNode = document.querySelector('.message');
            messageNode.textContent = '';
            if(xhr.readyState === 4 && xhr.status === 200) {
                var resp = JSON.parse(xhr.responseText)
                var authToken = resp.authToken;
                var email = resp.email;
                sessionStorage.setItem('currentUser', email.split('@')[0]);
                sessionStorage.setItem('authToken', JSON.stringify(authToken));
                window.location = '/categories.html';
            } else if (xhr.status === 401) {
                messageNode.textContent = 'Passwords do not match! Try again.';
            } else if (xhr.status === 409) {
                 messageNode.textContent = 'Email already taken!';
            }
         }
     }
}


