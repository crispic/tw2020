window.onload = () => {
  var authToken = sessionStorage.getItem('authToken');
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/login.html', true); // false for synchronous request
  xhr.send(null);
  xhr.onload = (e) => {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var resp = xhr.responseText;
      return resp;
    } else if (xhr.status === 404) {
      console.log('File not found');
    }
  };
};

function login() {
  let email = document.getElementById('email').value;
  let password = document.getElementById('password').value;
  post_data = {
    email: email,
    password: password,
  };
  let xhr = new XMLHttpRequest();
  xhr.open('POST', '/login.html', true);
  xhr.send(JSON.stringify(post_data));
  xhr.onload = (e) => {
    let messageNode = document.querySelector('.message');
    messageNode.textContent = '';
    if (xhr.readyState === 4 && xhr.status === 200) {
      var resp = JSON.parse(xhr.responseText);
      var authToken = resp.authToken;
      var email = resp.email;
      sessionStorage.setItem('currentUser', email.split('@')[0]);
      sessionStorage.setItem('authToken', JSON.stringify(authToken));
      window.location = '/categories.html';
    } else if (xhr.status === 401) {
      messageNode.textContent = 'Wrong credentials! Try again.';
    }
  };
}
