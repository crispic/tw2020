// sets the local storage variable "Category"
function setCategories(){
    getPage("/categories.html")
    var request = new XMLHttpRequest()
    var url = "/categoriesGet"
    request.open("GET", url)
    request.send()
    request.onreadystatechange=(e)=>{
        if(request.readyState === 4 && request.status === 200){
            var resp = JSON.parse(request.responseText)
            document.getElementById("catContainer").innerHTML = generateCategoriesHtml(resp);
        }
    }
}


function categoryButtonClick(id){
    console.log(id);
    addToStorage('local', "Category", id.toString());
    window.location.href = "/mainPage.html";
}
  