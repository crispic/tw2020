function resetTopic(){
    deleteFromStorage("local", "Topic")
    deleteFromStorage("local", "Questions")
    deleteFromStorage("local", "TopicId")
    deleteFromStorage("local", "Answers")
    deleteFromStorage("local", "Slides")
}

function getAccessLevel(catId){
    authToken = getFromStorage("session", "authToken").value
    var request = new XMLHttpRequest()
    var url = "/access_level?catId=" + catId + "&authToken=" + authToken
    request.open("GET", url)
        request.send()
        request.onreadystatechange=(e)=>{
            if(request.readyState === 4 && request.status === 200){
                var resp = JSON.parse(request.responseText)
                addToStorage("session", "Access_Level", resp["access_level"].toString())
            }
        }
}

function setTopics(){
    getPage("/Topics.html")
    debugger;
    var catId = getFromStorage('local', "Category")
    getAccessLevel(parseInt(catId.value))
    if(catId.status){
        var request = new XMLHttpRequest()
        var url = "/topics?catId=" + catId.value
        request.open("GET", url)
        request.send()
        request.onreadystatechange=(e)=>{
            if(request.readyState === 4 && request.status === 200){
                var resp = JSON.parse(request.responseText)
                document.getElementById("topicsGridContainer").innerHTML = generateTopicsHtml(resp);
            }
        }
    }
    else{
        window.location.href = "/categories.html"
    }
}

function getSlides(topicId){
    var catId = getFromStorage('local', "Category")
    addToStorage('local', "Topic", topicId)
    topicId = topicId.toString()
    if(catId.status){
        var request = new XMLHttpRequest()
        var url = "/slides?catId=" + catId.value + "&topicId=" + topicId
        request.open("GET", url)
        request.send()
        request.onreadystatechange=(e)=>{
            if(request.readyState === 4 && request.status === 200){
                addToStorage("local", "Slides", request.responseText)
            }
        }
    }
    else{
        window.location.href = "/categories.html"
    }
}

function getQuestions(){
    var catId = getFromStorage('local', "Category")
    var topicId = getFromStorage('local', "Topic")
    if(catId.status && topicId.status){
        var request = new XMLHttpRequest()
        var url = "/quiz?catId=" + catId.value + "&topicId=" + topicId.value
        request.open("GET", url)
        request.send()
        request.onreadystatechange=(e)=>{
            if(request.readyState === 4 && request.status === 200){
                addToStorage("local", "Questions", request.responseText)
                data = JSON.parse(request.responseText)
                answers = []
                for(i = 0; i < data.length; i++){
                    question = {}
                    question["catId"] = data[i].categoryId
                    question["topicId"] = data[i].topicId
                    question["id"] = data[i].id
                    question["answer"] = ""
                    answers[i] = question
                }
                addToStorage("local", "Answers", JSON.stringify(answers))
            }
        }
    }
}

function setQuestion(questionNum, questions){
    question = questions[questionNum - 1]
    document.getElementById("question").innerHTML = "<span id=\"questionNumber\">" + questionNum.toString() +".</span> " + question["question"]
    document.getElementById("question").question_id = question["id"]
    document.getElementById("answer1_label").innerHTML = question["answer_A"]
    document.getElementById("answer2_label").innerHTML = question["answer_B"]
    document.getElementById("answer3_label").innerHTML = question["answer_C"]
    document.getElementById("totalPages").innerHTML = question["totalQuestions"]
    document.getElementById("currentPage").innerHTML = questionNum
    if(questionNum == 1){
        document.getElementById("backBtnQuiz").style.display = "none"
    }
    else{
        document.getElementById("backBtnQuiz").style.display = "block"
    }
}

function setSlide(currentSlideNum, slides){
    var slidesCount = slides[0].totalSlides
    document.getElementById("totalPages").innerHTML = slidesCount;
    var slide;
    for(i = 0; i < slides.length; i++){
        if(slides[i].slideNum === currentSlideNum){
            slide = slides[i]
            break
        }
    }
    document.getElementById("topicsLectureContent").innerHTML = slide.slide
    document.getElementById("currentPage").innerHTML = currentSlideNum
    if(currentSlideNum == 1){
        document.getElementById("backBtnLecture").style.display = "none"
    }
    else{
        document.getElementById("backBtnLecture").style.display = "block"
    }
}

function lecturePopup(id){
    debugger;
    getSlides(id);
    addToStorage('local', "TopicId", id.toString())
    setSlide(1, JSON.parse(getFromStorage('local', "Slides").value))
    document.getElementById("backBtnQuiz").style.display = "none"
    document.getElementById("nextBtnQuiz").style.display = "none"
    var popup = document.getElementById("topicsPopupContainer")
    popup.style.display = "block";
}

function popupCloseBtn_onClick(){
    var popup = document.getElementById("topicsPopupContainer")
    popup.style.display = "none";
    resetTopic()
    window.location.reload()
}

function nextBtnLecture_onClick(){
    var nextPage = parseInt(document.getElementById("currentPage").innerHTML) + 1
    var totalPages = parseInt(document.getElementById("totalPages").innerHTML)
    if(nextPage == totalPages){
        debugger;
        var accessLevel = parseInt(getFromStorage("session", "Access_Level").value)
        var topicId = parseInt(getFromStorage("local", "TopicId").value)
        if(accessLevel > topicId){
            document.getElementById("nextBtnLecture").style.display = "none"
            setSlide(nextPage, JSON.parse(getFromStorage('local', "Slides").value))
        }
        else{
            document.getElementById("nextBtnLecture").style.display = "block"
            setSlide(nextPage, JSON.parse(getFromStorage('local', "Slides").value))
        }
    }
    else{
        if(nextPage > totalPages){
            getQuestions()
            setQuestion(1, JSON.parse(getFromStorage('local', "Questions").value))
            document.getElementById("backBtnQuiz").style.display = "block"
            document.getElementById("nextBtnQuiz").style.display = "block"
            document.getElementById("nextBtnLecture").style.display = "none"
            document.getElementById("backBtnLecture").style.display = "none"
            document.getElementById("topicsLectureContent").style.display = "none"
            document.getElementById("quizContentTopics").style.display = "block"
        }
        else{
            document.getElementById("nextBtnLecture").style.display = "block"
            setSlide(nextPage, JSON.parse(getFromStorage('local', "Slides").value))
        }
    }
}

function backBtnLecture_onClick(){
    var nextPage = parseInt(document.getElementById("currentPage").innerHTML) - 1
    setSlide(nextPage, JSON.parse(getFromStorage('local', "Slides").value))
    if(nextPage == totalPages){
        document.getElementById("nextBtnLecture").style.display = "none"
    }
    else{
        document.getElementById("nextBtnLecture").style.display = "block"
    }
}

function getAnswer(){
    if(document.getElementById("answer1").checked){
        return document.getElementById("answer1").value
    }
    if(document.getElementById("answer2").checked){
        return document.getElementById("answer2").value
    }
    if(document.getElementById("answer3").checked){
        return document.getElementById("answer3").value
    }
    return false

}

function setAnswer(question){
    var answers = JSON.parse(getFromStorage("local", "Answers").value)
    answers[question - 1]["answer"] = getAnswer()
    addToStorage("local", "Answers", JSON.stringify(answers))
}

function resetAnswers(){
    document.getElementById("answer1").checked = false
    document.getElementById("answer2").checked = false
    document.getElementById("answer3").checked = false
}

function checkAnswers(){
    var answers = JSON.parse(getFromStorage("local", "Answers").value)
    for(i=0; i<answers.length; i++){
        if(answers[i].answer != "A" && answers[i].answer != "B" && answers[i].answer != "C")
            return false
    }
    return true
}

function nextBtnQuiz_onClick(){
    var currentQuestion = parseInt(document.getElementById("currentPage").innerHTML)
    var allQuestions = parseInt(document.getElementById("totalPages").innerHTML)
    if(getAnswer()){
        setAnswer(currentQuestion)
    }
    if(currentQuestion < allQuestions){
        setQuestion(currentQuestion + 1, JSON.parse(getFromStorage("local", "Questions").value))
        resetAnswers()
    }
    else{
        if(checkAnswers()){
            var data = {}
            data.authToken = getFromStorage("session", "authToken").value
            data.answers = JSON.parse(getFromStorage("local", "Answers").value)
            var request = new XMLHttpRequest()
            var url = "/quizCheck"
            request.open("POST", url)
            request.send(JSON.stringify(data))
            request.onreadystatechange=(e)=>{
                if(request.readyState === 4 && request.status === 200){
                    var resp = JSON.parse(request.responseText)
                    if(resp.status){
                        alert("Congratulations, you passed this topic!")
                        resetTopic()
                        window.location.reload()
                    }
                    else{
                        if(resp.message == "not auth"){
                            window.location.href = "/entryPage.html"
                            resetTopic()
                        }
                        else{
                            alert("Unfortunately, you didn't answer all questions correctly.")
                            resetTopic()
                            window.location.reload()
                        }
                    }
                }
            }
        }
        else{
            alert("Please answer all the questions")
        }
    }
}

function backBtnQuiz_onClick(){
    var currentQuestion = parseInt(document.getElementById("currentPage").innerHTML)
    var allQuestions = parseInt(document.getElementById("totalPages").innerHTML)
    if(getAnswer()){
        setAnswer(currentQuestion)
    }
    setQuestion(currentQuestion - 1, JSON.parse(getFromStorage("local", "Questions").value))
    resetAnswers()
}

function extendNavbar() {
    var x = document.getElementById("myTopnav");
    if (x.className == "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }
  