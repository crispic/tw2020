function generateCategoriesHtml(categories){
    var a = "";
    for(i=0;i<categories.length;i++){
        var content = ""
        content += "<button id=\"" + categories[i].id.toString() + "\"";
        content += "class=\"button-container\" onclick=\"categoryButtonClick(this.id)\">"
        content += categories[i].name + "</button>"
        a += content;
    }
    return a;
}

function generateTopicsHtml(topics) {
    var a = "";
    for(i=0; i<topics.length; i++){
        var content = "";
        content += "<a class=\"topicLink\" ";
        var accessLevel = parseInt(getFromStorage("session", "Access_Level").value)
        if(i < accessLevel){
            content += "onclick=\"lecturePopup(this.id)\" "
        }
        content += "id=\"" + topics[i].id + "\"><div class=\"topic\">"
        content += "<img src=\"" + topics[i].imagePath + "\" alt=\"topicImage\" class=\"topicImage\" />"
        content += "<span class=\"topicTitle\">" + topics[i].name + "</span></div></a>"
        a += content
    }
    return a;
}