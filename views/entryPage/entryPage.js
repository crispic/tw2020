window.onload = () => {
    var authToken = sessionStorage.getItem('authToken');
    var xhr = new XMLHttpRequest();
    xhr.open( "GET", '/entryPage.html', true ); // false for synchronous request
    xhr.send( null );
    xhr.onload = (e) => {
    if(xhr.readyState === 4 && xhr.status === 200) {
        var resp = xhr.responseText;
        return resp;
    } else if (xhr.status === 404) {
        console.log('File not found');
    }
  }
}