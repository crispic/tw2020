var rssData;

window.onload = () => {
  getPage('/topPlayers.html');
};

function getRSSData() {
    parser = new DOMParser();
    let xmlDoc = parser.parseFromString(rssData, 'text/xml');
    let rss = xmlDoc.getElementsByTagName('rss')[0];
    var request = new XMLHttpRequest();
    var url = "/topPlayers.html?rss=" + rssData;
    request.open("GET", url);
    request.send();
    request.onreadystatechange=(e)=>{
      if(request.readyState === 4 && request.status === 200){
          var resp = JSON.parse(request.responseText);
          console.log(resp);
      }
    }
 }

function extendNavbar() {
  var x = document.getElementById('myTopnav');
  if (x.className == 'topnav') {
    x.className += ' responsive';
  } else {
    x.className = 'topnav';
  }
}

function generateTopHtml(top) {
  var a = '';

  // Adding the progress bar
  a += '';

  for (i = 0; i < top.length; i++) {
    var content = '';
    content += '<div class="player">';
    if (top[i].gender == false)
      content += '<img alt="femaleAvatar" src="/female-avatar.png" />';
    else content += '<img alt="maleAvatar" src="/male-avatar.png" />';
    content += '<div class="name">' + top[i].username + '</div>';
    content += '<ul>';
    content += '<li class="top">Top: ' + top[i].top + '</li>';
    content += '<li class="progress">Progress: ' + top[i].progress + '%</li>';
    content += '<li class="score">Score: ' + top[i].score + '</li>';
    content += '<li class="badges">Badges: ' + top[i].badges + '</li>';
    content += '</ul> \n </div>';

    a += content;
  }
  return a;
}

function setRssData(data){
    rssData = data;
}

function setTop() {
  getPage('/topPlayers.html');
  var request = new XMLHttpRequest();
  var url = '/topGet';
  request.open('GET', url);
  request.send();
  request.onreadystatechange = (e) => {
    if (request.readyState === 4 && request.status === 200) {
      var resp = JSON.parse(request.responseText);
      document.getElementById('topContainer').innerHTML = generateTopHtml(resp);
      let data = resp;
      let rssData = generateXML(data);
      var s = new XMLSerializer();
       var newXmlStr = s.serializeToString(rssData);
      setRssData(newXmlStr);
    }
  };
}

function generateXML(data) {
    let doc = document.implementation.createDocument('', '', null);
    let xml_tag = doc.createElement('xml');
    xml_tag.setAttribute('version', '1.0');
    xml_tag.setAttribute('encoding', 'utf-8');
    let rss = doc.createElement('rss');
    rss.setAttribute('version', '2.0');
    xml_tag.appendChild(rss);
    let channel = doc.createElement('channel');
    let channelTitle = doc.createElement('title');
    channelTitle.innerHTML = document.querySelector('title').innerHTML;
    let channelLink = doc.createElement('link');
    channelLink.innerHTML = 'http://localhost:4000/topPlayers.html';
    let channelDescription = doc.createElement('description');
    channelDescription.innerHTML = 'Top players(updates)';
    rss.appendChild(channel);
    channel.appendChild(channelTitle);
    channel.appendChild(channelLink);
    channel.appendChild(channelDescription);
    data.forEach(obj => {
        let item = doc.createElement('item');
        channel.appendChild(item);
        let player_name = obj.username;
        let player_top = obj.top;
        let player_progress = obj.progress;
        let player_score = obj.score;
        let player_badges = obj.badges;
        let item_title = doc.createElement('title');
        item_title.innerHTML = player_name;
        let item_description = doc.createElement('description');
        item_description.innerHTML = 'Player ' + player_name + ' is currently on position ' + player_top
            + ' having a progress of ' + player_progress + '%, a score of ' + player_score
            + ' and a number of ' + player_badges + ' badges.'
        item.appendChild(item_title);
        item.appendChild(item_description);
    });
    return rss;
}
