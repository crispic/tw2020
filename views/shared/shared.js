//Utility functions

/*
    Storage functions
    location: 'session' or 'local'; depending on the storage used
    key: the key of the pair
    value: the value of the pair
*/

//Gets the storage object
function getStorage(location){
    if(location === "local"){
        return window.localStorage;
    }
    if(location === "session"){
        return window.sessionStorage
    }
    return undefined;
}

//Sets a key-value pair in the storage
function addToStorage(location, key, value){
    let storage = getStorage(location);
    storage.setItem(key, value);
}

//Gets a key-value pair form the storage or undefined if the pair doesn't exist
function getFromStorage(location, key){
    let result = {status: false, value: undefined}
    let storage = getStorage(location);
    let value = storage.getItem(key);
    if(value == undefined || value == null){
        return result;
    }
    else{
        result.status = true;
        result.value = value;
        return result;
    }
}

function deleteFromStorage(location, key){
     let storage = getStorage(location);
     storage.removeItem(key)
}

// Verifies user token and then
// Sets message for user or redirects him to entryPage if he is not logged
function getPage(url) {
    var authToken = sessionStorage.getItem('authToken');
    var currentUser = sessionStorage.getItem('currentUser');
    var xhr = new XMLHttpRequest();
    let validateUserTokenUrl = '/validateUser?authToken=' + authToken;
    xhr.open( "GET", validateUserTokenUrl, true );
    xhr.send( null );
    xhr.onload = (e) => {
        if(xhr.readyState === 4 && xhr.status === 200) {
            xhr.open( "GET", url, true );
            xhr.send( null );
            xhr.onload = (e) => {
            let messageNode = document.querySelector('.user-message');
            if(messageNode) messageNode.textContent = '';
            if(xhr.readyState === 4 && xhr.status === 200) {
                if(url === '/categories.html') {
                    if(messageNode) messageNode.textContent = `Hello, ${currentUser}! Choose a category!`;
                } else {
                    if(messageNode) messageNode.textContent = `${currentUser}`;
                }
                var resp = xhr.responseText;
                return resp;
            } else if (xhr.status === 404) {
                    console.log('File not found');
                }
            }
        } else {
            if(sessionStorage.getItem('authToken')) {
                sessionStorage.removeItem('authToken');
                sessionStorage.removeItem('currentUser');
                window.location = '/redirectPage.html';
            }
        }
    }
}

function logout() {
    if(sessionStorage.getItem('authToken')) {
        sessionStorage.removeItem('authToken');
        sessionStorage.removeItem('currentUser');
        window.location = '/entryPage.html';
    }
}
