import sqlite3


def execute_command(command):
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    cursor.execute(command)
    db.commit()


def execute_command2(command, values):
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    cursor.execute(command, values)
    db.commit()


def create_users():
    command = '''
CREATE TABLE IF NOT EXISTS users(
id INTEGER PRIMARY KEY,
email VARCHAR(40) NOT NULL,
password VARCHAR(25) NOT NULL,
password VARCHAR(25) NOT NULL)
'''
    execute_command(command)


def create_auth_table():
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    command = '''
CREATE TABLE IF NOT EXISTS auth_table(
id INTEGER PRIMARY KEY,
user_id INTEGER NOT NULL,
auth_token VARCHAR(40) NOT NULL)
'''
    execute_command(command)


create_auth_table()


def update_auth_token(user_id, auth_token):
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    command = '''UPDATE auth_table SET auth_token=:auth_token WHERE user_id=:user_id'''
    cursor.execute(command, {'auth_token': auth_token, 'user_id': user_id})
    db.commit()


def add_auth_user(user_id, auth_token):
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    command = '''INSERT INTO auth_table(user_id, auth_token) VALUES(:user_id, :auth_token)'''
    cursor.execute(command, {'user_id': user_id, 'auth_token': auth_token})
    db.commit()


def get_auth_token(user_id):
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    get_token = 'SELECT auth_token from auth_table WHERE user_id=:user_id'
    cursor.execute(get_token, {'user_id': user_id})
    results = cursor.fetchall()
    if results:
        return True
    else:
        return False

# get_auth_token(1)


def verify_auth_token(authToken):
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    get_token = 'SELECT auth_token from auth_table WHERE auth_token=:authToken'
    cursor.execute(get_token, {'authToken': authToken})
    results = cursor.fetchall()
    if results:
        return True
    else:
        return False

# print('verifyyy', verify_auth_token('236089277e28b4cca90eff7e0ed7417b'))

def get_user_id(email):
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    get_token = 'SELECT * from users WHERE email=:email'
    cursor.execute(get_token, {'email': email})
    results = cursor.fetchall()
    if results:
        return results[0][0]
    else:
        return False

# print('id', getUserId('cristina.piclea@yahoo.com'))


def delete_user_by_email(email):
    command = "DELETE FROM users WHERE email LIKE \'" + email + "\'; "
    execute_command(command)


def delete_user_by_id(user_id):
    command = "DELETE FROM users WHERE email LIKE \'" + user_id + "\'; "
    execute_command(command)


def delete_logged_user(email):
    command = "DELETE FROM logged_users WHERE email LIKE \'" + email + "\'; "
    execute_command(command)


def auth_user(id):
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    command = '''DELETE FROM auth_table
    WHERE id LIKE id; '''
    cursor.execute(command)
    db.commit()


# auth_user(1)
# delete_user('cristina.piclea@yahoo.com')


def create_badges():
    command = '''
CREATE TABLE IF NOT EXISTS badges(
id INTEGER PRIMARY KEY,
name VARCHAR(40))
'''
    execute_command(command)


def create_quizzes():
    command = '''
CREATE TABLE IF NOT EXISTS quizzes(
id INTEGER PRIMARY KEY,
name VARCHAR(40),
score INTEGER)
'''
    execute_command(command)


def create_users_badges():
    command = '''
CREATE TABLE IF NOT EXISTS users_badges(
id INTEGER PRIMARY KEY,
user_id INTEGER,
badge_id INTEGER)
'''
    execute_command(command)


def create_users_quizzes():
    command = '''
CREATE TABLE IF NOT EXISTS users_quizzes(
id INTEGER PRIMARY KEY,
user_id INTEGER,
quiz_id INTEGER,
score INTEGER)
'''
    execute_command(command)


def create_categories():
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = ''' CREATE TABLE IF NOT EXISTS categories(
    id INTEGER PRIMARY KEY,
    name TEXT
    )'''
    cursor.execute(command)
    db.commit()


def create_quiz():
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = ''' CREATE TABLE IF NOT EXISTS quiz(
    id INTEGER PRIMARY KEY,
    categoryId INTEGER,
    topicId INTEGER,
    question TEXT,
    answer_A TEXT,
    answer_B TEXT,
    answer_C TEXT,
    correctAnswer TEXT
    )'''
    cursor.execute(command)
    db.commit()


def create_topics():
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = '''CREATE TABLE IF NOT EXISTS topics(
        id INTEGER PRIMARY KEY,
        categoryId INTEGER,
        name TEXT,
        imagePath TEXT
    )'''
    cursor.execute(command)
    db.commit()


def create_slides():
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = '''CREATE TABLE IF NOT EXISTS slides(
        id INTEGER PRIMARY KEY,
        categoryId INTEGER,
        topicId INTEGER,
        slide TEXT,
        slideNum INTEGER
    )'''
    cursor.execute(command)
    db.commit()


def add_user(email, password):
    command = "INSERT INTO users(email, password) VALUES(:email, :password)"
    values = {'email': email, 'password': password}
    execute_command2(command, values)


def add_badge(name):
    command = 'INSERT INTO badges(name) VALUES(:name)'
    values = {'name': name}
    execute_command2(command, values)


def add_badge_to_user(user_id, badge_id):
    command = 'INSERT INTO users_badges(user_id, badge_id) VALUES(:user_id, :badge_id)'
    values = {'user_id': user_id, 'badge_id': badge_id}
    execute_command2(command, values)


def add_quiz(name, score):
    command = 'INSERT INTO quizzes(name, score) VALUES(:name, :score)'
    values = {'name': name, 'score': score}
    execute_command2(command, values)


def print_table(table_name):
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    command = 'SELECT * FROM ' + table_name
    cursor.execute(command)
    for i in cursor.fetchall():
        print(i)


def print_tables():
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    command = 'SELECT name FROM sqlite_master'
    cursor.execute(command)
    for i in cursor.fetchall():
        print(i)


def add_session():
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    command = 'INSERT INTO logged_users(email, userId, auth_token) Values(\'abc@yahoo.com\', 5, \'abcdefg\')'
    cursor.execute(command)
    db.commit()


def logged_users():
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    command = '''
        CREATE TABLE IF NOT EXISTS logged_users(
        id INTEGER PRIMARY KEY,
        email VARCHAR(40) NOT NULL,
        userId INTEGER,
        auth_token VARCHAR(200) NOT NULL)
        '''
    cursor.execute(command)
    db.commit()


def create_quiz_score():
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    command = '''
        CREATE TABLE IF NOT EXISTS quiz_scores(
        id INTEGER PRIMARY KEY,
        userId INTEGER,
        categoryId INTEGER,
        topicId INTEGER,
        score INTEGER)
        '''
    cursor.execute(command)
    db.commit()


def create_access():
    with sqlite3.connect('GaL.db') as db:
        # cursor to perform actions on the db
        cursor = db.cursor()
    command = '''
        CREATE TABLE IF NOT EXISTS user_access_level(
        id INTEGER PRIMARY KEY,
        userId INTEGER,
        categoryId INTEGER,
        topicId INTEGER)
        '''
    cursor.execute(command)
    db.commit()


create_access()
create_badges()
create_users_badges()

# --- CRUD.py


# Categories
def categories_read():
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = 'SELECT * FROM categories'
    cursor.execute(command)
    categories = []
    for i in cursor.fetchall():
        categories.append({"id": i[0], "name": i[1]})
    return categories


def categories_create(values_dict):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = 'INSERT INTO categories(id, name) VALUES(:id, :name)'
    cursor.execute(command, values_dict)
    db.commit()


# Topics
def topics_read(catId):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = 'SELECT * FROM topics WHERE categoryId = :catId'
    cursor.execute(command, {"catId": str(catId)})
    topics = []
    for i in cursor.fetchall():
        topics.append({"id": i[0], "categoryId": i[1], "name": i[2], "imagePath": i[3]})
    return topics


def topics_create(values_dict):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = 'INSERT INTO topics(id, categoryId, name, imagePath) VALUES(:id, :categoryId, :name, :image_path)'
    cursor.execute(command, values_dict)
    db.commit()


# Slides
def slides_read(catId, topicId):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = 'SELECT slide, slideNum FROM slides ' \
              'WHERE categoryId = :catId AND topicId = + :topicId'
    slides = []
    cursor.execute(command, {'catId': str(catId), 'topicId': str(topicId)})
    for i in cursor.fetchall():
        slides.append({"totalSlides": 0, "slide": i[0], "slideNum": i[1]})
    for j in range(0, len(slides)):
        slides[j]["totalSlides"] = len(slides)
    return slides


def slides_create(values_dict):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = 'INSERT INTO slides(id, categoryId, topicId, slide, slideNum) ' \
              'VALUES(:id, :categoryId, :topicId, :slide, :slideNum)'
    cursor.execute(command, values_dict)
    db.commit()


# Quiz
def quiz_read(catId, topicId):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = 'SELECT id, question, answer_A, answer_B, answer_C, categoryId, topicId FROM quiz ' \
              'WHERE categoryId = :catId AND topicId = :topicId ORDER BY id ASC'
    questions = []
    cursor.execute(command, {"catId": str(catId), "topicId": str(topicId)})
    for i in cursor.fetchall():
        questions.append({"totalQuestions": 0, "id": i[0], "question": i[1], "answer_A": i[2], "answer_B": i[3], "answer_C": i[4], "categoryId": i[5], "topicId": i[6]})
    for j in range(0, len(questions)):
        questions[j]["totalQuestions"] = len(questions)
    return questions


def quiz_create(values_dict):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = 'INSERT INTO quiz(id, categoryId, topicId, question, answer_A, answer_B, answer_C, correctAnswer) ' \
              'VALUES(:id, :categoryId, :topicId, :question, :answer_A, :answer_B, :answer_C, :correctAnswer)'
    cursor.execute(command, values_dict)
    db.commit()


def quiz_getRightAnswer(id):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = 'SELECT correctAnswer FROM quiz WHERE id= :id'
    cursor.execute(command, {"id": str(id)})
    answer = ""
    for i in cursor.fetchall():
        answer = i[0]
    return answer


# Logged Users
def loggedUsers_GetId(token):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = 'SELECT user_id FROM auth_table WHERE auth_token= :token'
    cursor.execute(command, {"token": token})
    userId = ""
    for i in cursor.fetchall():
        userId = i[0]
    return userId


# Quiz Scores
def quiz_score_create(dict_values):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = "INSERT INTO quiz_scores(userId, categoryId, topicId, score) VALUES(" + str(dict_values["userId"]) + ", " \
              + str(dict_values["categoryId"]) + ", " + str(dict_values["topicId"])\
              + ", " + str(dict_values["score"]) + ")"
    cursor.execute(command)
    db.commit()


# Access Level
def access_level_get(categoryId, userId):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = "SELECT topicId FROM user_access_level WHERE userId=:userId AND categoryId=:categoryId"
    cursor.execute(command, {"userId": userId, "categoryId": categoryId})
    topicId = 0
    for i in cursor.fetchall():
        topicId = i[0]
    return topicId


def access_level_update(categoryId, userId, topicId):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = "UPDATE user_access_level SET topicId=:topicId WHERE userId=:userId AND categoryId=:categoryId"
    cursor.execute(command, {"categoryId": categoryId, "userId": userId, "topicId": topicId})
    db.commit()


def access_level_create(userId, categoryId, topicId):
    with sqlite3.connect('GaL.db') as db:
        cursor = db.cursor()
    command = "INSERT INTO user_access_level(userId, categoryId, topicId) VALUES(:userId, :categoryId , :topicId)"
    cursor.execute(command, {"userId": userId, "categoryId": categoryId, "topicId": topicId})
    db.commit()


def newUser_createAccessLevel(userId):
    categories = categories_read()
    for i in categories:
        access_level_create(userId, i["id"], 1)

# Categories
# categories_create({"id": 1, "name": "Basics"})
# categories_create({"id": 2, "name": "PvE"})
# categories_create({"id": 3, "name": "PvP"})
# categories_create({"id": 4, "name": "Farming"})
# categories_create({"id": 5, "name": "End Game"})


# Topics
# topics_create({"id": 1, "categoryId": 1, "name": "Character Creation", "image_path": "basics_character-creation.png"})
# topics_create({"id": 2, "categoryId": 1, "name": "Races", "image_path": "basics_races.png"})
# topics_create({"id": 3, "categoryId": 1, "name": "Classes", "image_path": "basics_classes.png"})
# topics_create({"id": 4, "categoryId": 1, "name": "Quests", "image_path": "basics_quests.png"})
# topics_create({"id": 5, "categoryId": 1, "name": "Combat", "image_path": "basics_combat.png"})
# topics_create({"id": 6, "categoryId": 1, "name": "Leveling Up", "image_path": "basics_leveling-up.png"})
# topics_create({"id": 7, "categoryId": 2, "name": "Introductions", "image_path": "pve_introduction.png"})
# topics_create({"id": 8, "categoryId": 2, "name": "Combat Roles", "image_path": "pve_combat-roles.png"})
# topics_create({"id": 9, "categoryId": 2, "name": "Dungeons", "image_path": "pve_dungeons.png"})
# topics_create({"id": 10, "categoryId": 3, "name": "Introduction", "image_path": "pvp_introduction.png"})
# topics_create({"id": 11, "categoryId": 3, "name": "Arenas", "image_path": "pvp_arenas.png"})
# topics_create({"id": 12, "categoryId": 3, "name": "Battlegrounds", "image_path": "pvp_battlegrounds.png"})
# topics_create({"id": 13, "categoryId": 4, "name": "Professions", "image_path": "farming_professions.png"})
# topics_create({"id": 14, "categoryId": 4, "name": "Reputation farming", "image_path": "farming_reputation.png"})
# topics_create({"id": 15, "categoryId": 4, "name": "Gold farming", "image_path": "farming_gold-farming.png"})
# topics_create({"id": 16, "categoryId": 5, "name": "End Game introduction", "image_path": "end-game_introduction.png"})
# topics_create({"id": 17, "categoryId": 5, "name": "End Game PvE", "image_path": "end-game_pve.png"})
# topics_create({"id": 18, "categoryId": 5, "name": "End Game PvP", "image_path": "end-game_pvp.png"})


# Slides
# slides_create({"id": 1, "categoryId": 1, "topicId": 1, "slide": "<br /><h1>General overview</h1><hr /><ul><li><p>After loging in with your Battle.net account, you will be presented with the character creation screen.In here, you will be able to view your character model as it will appear in the game wearing advanced equipment. On the left and right, you can choose your race and, respectively, class(more on those later), and at the bottom of the screen, you will find two buttons, More Info and Customize. </p></li><li><p>The More Info button will open two new windows with a description and some of the lore behind your class and race choices.</p></li><li><p>The Customize button will take you to a new screen in which you can customize the look of your character.</p></li></ul>", "slideNum": 1})
# slides_create({"id": 2, "categoryId": 1, "topicId": 1, "slide": "<br /><h1>Character customization</h1><hr /><ul><li><p>In the character customization window, you will see two columns on the left and right side of the screen. Ont top of the right column, there is the option to change genders. On the right column itself, you will see the body options you can customize, while in the left column, you will see the options themselves.</p></li><li><p>What body parts you can customize depend on the class and race you have chosen to play as. For example, some races like the Tauren, will have a Horn option and the Demon Hunter class will have the Blindfold and Tatoos option. Some of the choices that are persistent across race and class are Skin Color, Face, Hair Style and Hair Color.</p></li><li><p>Finally at the bottom, you have the Name field. Names must only have letters and must unique to the character. As such, your first choice of name is possibly taken... and your second one... and maybe your third one... This is why, under the text field for the Name, there is a Randomize button that will generate random, lore friendly, name(although it might also be taken).</p></li></ul>", "slideNum": 2})
# slides_create({"id": 3, "categoryId": 1, "topicId": 1, "slide": "<br /><h1>Ending remarks</h1><hr /><ul><li><p>In this topic we took a look at one side of the character creation process, and namely, the visual customization part. In the following two topics we will take a look at the playable races and classes in the game and we will describe what they do. And for now, quiz time!</p></li></ul>", "slideNum": 3})
# slides_create({"id": 4, "categoryId": 1, "topicId": 2, "slideNum": 1, "slide": "<br /><h1>Introduction</h1><hr /><ul><li><p>In World of Warcraft, your character is part of a faction, the Alliance or the Horde. The race you choose to play as is tied to one of those factions.</p></li><li><p>The race you choose dictates, for the most part, which class you can be. Each race has an active and some passive abilities, like how Night Elves have increased movement speedand how Taurens have 15 bonus points in herbalism. However, we assume this is your first time diving into WoW, and, as such, advise you to not think too much about these, as they will not matteruntil you reach the end game, and even then, not very much.</p></li><li><p>There is, however, an exception to the factions. Pandarens are, officially, neutral, however, at the end of their starting zone, they are forced to choose between the Alliance and Horde.Theoretically, it is possible to remain neutral, as you can gain experiecnce from certain professions, but, seeing as the process of leveling up in this case is extremely tedious, we do not reccomend that.</p></li><li><p>In the following slides, we will present the races of each faction, including their starting zones, capital city and available classes, which we will discuss in the next topic.</p></li></ul>"})
# slides_create({"id": 5, "categoryId": 1, "topicId": 2, "slideNum": 2, "slide": "<br /><h1>Alliance Races</h1><hr /><ul><li><p><b>Humans:</b><br>Capital City: Stormwind<br>Starting Zone: Elwynn Forest<br>Available classes: Hunter, Mage, Paladin, Priest, Rogue, Warlock, Warrior, Death Knight, Monk</p></li><li><p><b>Dwarves:</b><br>Capital City: Ironforge<br>Starting Zone: Dun Morogh<br>Available classes: Hunter, Mage, Paladin, Priest, Rogue, Shaman, Warlock, Warrior, Death Knight, Monk</p></li><li><p><b>Gnomes:</b><br>Capital City: New Tinkertown<br>Starting Zone: Dun Morogh<br>Available classes: Hunter, Mage, Priest, Rogue, Warlock, Warrior, Death Knight, Monk</p></li><li><p><b>Night Elves:</b><br>Capital City: Darnassus<br>Starting Zone: Teldrassil<br>Available classes: Druid, Hunter, Mage, Priest, Rogue, Warrior, Death Knight, Monk, Demon Hunter</p></li><li><p><b>Draenai:</b><br>Capital City: The Exodar<br>Starting Zone: Azuremyst Isle<br>Available classes: Hunter, Mage, Paladin, Priest, Shaman, Warrior, Death Knight, Monk</p></li><li><p><b>Worgen:</b><br>Capital City: Gilneas, however, after finishing the starting zone questline, it becomes Darnassus or Stormwind<br>Starting Zone:Gilneas<br>Available classes: Druid, Hunter, Mage, Priest, Rogue, Warlock, Warrior, Death Knight</p></li></ul>"})
# slides_create({"id": 6, "categoryId": 1, "topicId": 2, "slideNum": 3, "slide":"<br /><h1>Horde Races</h1><hr /><ul><li><p><b>Orcs:</b><br>Capital City: Orgrimmar<br>Starting Zone: Durotar<br>Available classes: Hunter, Mage, Rogue, Shaman, Warlock, Warrior, Death Knight, Monk</p></li><li><p><b>Tauren:</b><br>Capital City: Thunder Bluff<br>Starting Zone: Mulgore<br>Available classes: Druid, Hunter, Paladin, Priest, Shaman, Warrior, Death Knight, Monk</p></li><li><p><b>Undead:</b><br>Capital City: Undercity<br>Starting Zone: Tirisfal Glades<br>Available classes: Hunter, Mage, Priest, Rogue, Warlock, Warrior, Death Knight, Monk</p></li><li><p><b>Trolls:</b><br>Capital City: Durotar<br>Starting Zone: Orgrimmar<br>Available classes: Druid, Hunter, Mage, Priest, Rogue, Shaman, Warlock, Warrior, Death Knight, Monk</p></li><li><p><b>Blood Elves:</b><br>Capital City: Silvermoon City<br>Starting Zone: Eversong Woods<br>Available classes: Hunter, Mage, Paladin, Priest, Rogue, Warlock, Warrior, Death Knight, Monk, Demon Hunter</p></li><li><p><b>Goblins:</b><br>Capital City: Bilgewater Harbor, however, after finishing the starting zone questline, it becomes Orgrimmar or Undercity<br>Starting Zone: Kezan<br>Available classes: Hunter, Mage, Priest, Rogue, Shaman, Warlock, Warrior, Death Knight</p></li></ul>"})
# slides_create({"id": 7, "categoryId": 1, "topicId": 2, "slideNum": 4, "slide":"br /><h1>Neutral Races</h1><hr /><ul><li><p><b>Pandarens:</b><br>Capital City: Stormwind or Orgrimmar, depending on faction<br>Starting Zone: The Wandering Halls<br>Available classes: Hunter, Mage, Priest, Rogue, Shaman, Warrior, Monk</p></li><li><p>And this concludes our look at all the playeble races, excluding Allied Races. However, considering those are locked behind quest lines and reputation walls, we believe they are not something a new player should concern themselves with. In the next topic, we will look at the playable classes available in the game, but before that, quiz time!</p></li></ul>"})
# slides_create({"id": 8, "categoryId": 1, "topicId": 3, "slideNum": 1, "slide": "<br /><h1>Introduction</h1><hr /><ul><li><p>Classes are the main characteristic that will define your playstyle. Almost all of the abilities that you will use are class specific.</p></li><li><p>Each class has 3 advanced specialization options, with the exception of Druids, which have 4, and Demon Hunters, with two. Esch specialization, or spec, has a combat role, a notion explained in the PvE category.</p></li><li><p>The classess can also be categorized by normal and heroic classes. Unlike regular classess, heroic classes must be unlocked by having a character on your account above a certain level, start at an advanced level and there is a limit on how many charcters with that class you can have.</p></li><li><p>In the following slides, we will present the regular classes and then the heroic classes, in which we will mention their specialization and the coresponding role, and, in the case of heroic classes, the unlock requirments and character limits</p></li></ul>"})
# slides_create({"id": 9, "categoryId": 1, "topicId": 3, "slideNum": 2, "slide":"<br /><h1>Regular classes</h1><hr /><ul><li><p><b>Druids:</b><br>Specs: Balance(ranged damage), Feral(melee damage), Guardian(tanking), Restoration(healing)</p></li><li><p><b>Hunter:</b><br>Specs: Beast Mastery(ranged damage), Marksmanship(ranged, damage), Survival(melee damage)</p></li><li><p><b>Mage:</b><br>Specs: Arcane(ranged damage), Fire(ranged damage), Frost(ranged damage)</p></li><li><p><b>Paladin:</b><br>Specs: Holy(healing), Protection(tanking), Retribution(melee damage)</p></li><li><p><b>Priest:</b><br>Specs: Discipline(healing), Holy(healing), Shadow(ranged damage)</p></li><li><p><b>Rogue:</b><br>Specs: Assassination(melee damage), Outlaw(melee damage), Subtlety(melee damage)</p></li><li><p><b>Shaman:</b><br>Specs: Elemental(ranged damage), Enhancement(melee damage), Restoration(healing)</p></li><li><p><b>Warlock:</b><br>Specs: Affliction(ranged damage), Demonology(ranged damage), Destruction(ranged damage)</p></li><li><p><b>Warrior:</b><br>Specs: Arms(melee damage), Fury(melee damage), Protection(tanking)</p></li><li><p><b>Monk:</b><br>Specs: Windwalker(melee damage), Mistweaver(healing), Brewmaster(tanking)</p></li>"})
# slides_create({"id": 10, "categoryId": 1, "topicId": 3, "slideNum": 3, "slide":"<br /><h1>Heroic classes and closing remarks</h1><hr /><ul><li><p><b>Death Knights:</b><br>Specs: Blood(tanking), Frost(melee damage), Unholy(melee damage)<br>Unlock requirement: Have a character that is over level 55<br>Starting level: 55</p></li><li><p><b>Death Knights:</b><br>Specs: Vengeance(tanking), Havoc(melee damage)<br>Unlock requirement: Have a character that is over level 98<br>Starting level: 98</p></li><li><p>These are the current playable classess in the game. We will not tell you which class is the best, because they all excel at something. However we urge you to choose a class that has access to all three combat roles so that you can try them all and see what you enjoy playing.</p></li><li><p>Now that you have chosen your race, class and customized your character, you are ready to start your journey in Azeroth! But first, quiz time</p></li>"})
# slides_create({"id": 11, "categoryId": 1, "topicId": 4, "slideNum": 1, "slide": "<br /><h1>Introduction</h1><hr /><ul><li><p>As soon as you create your character, you will see a cutscene which explains a bit about your race choice. After this, you will spawn in the starting zone for your race and you will see an NPC(non-player character), with a golden exclamation mark above their heads. That character has a quest for you.</p></li></ul>"})
# slides_create({"id": 12, "categoryId": 1, "topicId": 4, "slideNum": 2, "slide": "<br /><h1>Quests</h1><hr /><ul><li><p>Quests are the main way in which you will experience the story and progression system of World of Warcraft. They can range from delivering a simple message to eliminating a set ammount of enemies.</p></li><li><p>The rewards depend on the difficulty of the quest. For example, delivering a message will not be as profitable as gathering a number of materials. But what is allways true is the fact that quests reward you with experience points, gold and, sometimes, equioment appropiatefor the level of the quest.</p></li><li><p>Quests are, also, a way to experience the zone questline, as, before accepting a quest, a window will pop up with what the quest giver supposedly says to your character about the quest.</p></li></ul>"})
# slides_create({"id": 13, "categoryId": 1, "topicId": 4, "slideNum": 3, "slide": "<br /><h1>Quest chains and quest hubs</h1><hr /><ul><li><p>As mentioned in the previous slide, the storyline of a zone can be experienced through quests. These quests are art of something that is called a quest chain.</p></li><li><p>Quest chains are a series of quests that are connected to one another. Each quest becomes available as soon as the previous quest is completed and the player character has met the level requirements. The start of a zone quest chain can be found, ussually, in the first settlement of a zone, surrounded by other quest givers. Such a settlement is called a quest hub.</p></li><li><p>Quest hubs are places in which a player character can pick up multiple quests and, after finishing them, turn the quests in. Each zone has a few of them, around 2 to 4, depending of the leveling range of the zone.</p></li><li><p>All in all, quests are the main way you will gain experience points and, as such, progress through the game. In the next topic we will take a look at the combat system of this game. But for now, quiz time!</p></li></ul>"})
# slides_create({"id": 14, "categoryId": 1, "topicId": 5, "slideNum": 1, "slide": "<br /><h1>Introduction</h1><hr /><ul><li><p>In World of Warcraft, your character fights through using his/hers skills. Which skills you have is dependent on the race and class you chose during character creation, as well as your active specialization.</p></li><li><p>While you have access to skills that deal damage, you will, eventually get learn skills that stun, slow and intrerupt a target. Classes that have tanking specs also have access to skills that buff their defenses and that pull a target's attention to them(Taunt), while classes that have healing specs have skills that heal themselves and others.</p></li><li><p>In the next slide, we will take a look at combat modifiers that you can inflict on enemies and they will be able to inflict on you.</p></li></ul>"})
# slides_create({"id": 15, "categoryId": 1, "topicId": 5, "slideNum": 2, "slide": "<br /><h1>Combat modifiers</h1><hr /><ul><li><p><b>1) Slowed:</b> The target is slowed, but can still move and use skills.</p></li><li><p><b>2) Rooted:</b> The target can't move, but can still use skills.</p></li><li><p><b>3) Stunned:</b> The target can't move and can't use skills.</p></li><li><p><b>4) Damage over Time(DoT):</b> The target receives periodic damage for set ammount of time.</p></li><li><p><b>5) Heling over Time:</b> The target is healed periodically for a set ammount of time.</p></li><li><p><b>6) Confused:</b> The player loses control of their character as they wander confused.</p></li></ul>"})
# slides_create({"id": 16, "categoryId": 1, "topicId": 5, "slideNum": 3, "slide": "<br /><h1>Cooldowns and closing remarks</h1><hr /><ul><li><p>You may notice that some of your abilities have something called cooldown, in their description. This means that after using the skill, you'll have to wait to use it again. This is called active cooldown.</p></li><li><p>Aside from active cooldowns, there is also a concept called global cooldown. This is the time it takes to cast abilities between them. Most abilities are affected by the global cooldown, the exception being some buffs and removal of movement effects, altough those ussually have an active cooldown.</p></li><li><p>The key to mastering combat using your class is to know your abilities and find a way to manage your cooldowns, as well as knowing how to react in every situation you might be placed in.</p></li><li><p>For now, these are the basics of combat, on which we will expand in the PvP and PvE categories And for now, quiz time!</p></li></ul>"})
# slides_create({"id": 17, "categoryId": 1, "topicId": 6, "slideNum": 1, "slide": "<br /><h1>Leveling up</h1><hr /><ul><li><p>By now, you should have completed some quests and gotten some experience points. While doing this, your character might have been surrounded by a golden light, and the experience bar on the bottom of your screen has reset. This means that you have leveled up.</p></li><li><p>When you level up, you obtain some points in several stats used by your class, get access to more quests, can wear better equipment and, at certain levels, unlock new skills.</p></li><li><p>If you open your map, at the top of the window, where you see the name of the zone you are in, you will see 2 numbers in paranthesis. This is the level range of the zone. After you reach the max level on this range, you should move on to the next zone to continue to level up as quickly as possible. You can see which zone you should go to next by checking the Adventure Guide.</p></li><li><p>The max level in World of Warcraft depends on the expansion you have access to. In the next slide, we will list the max level available in each expansion released at the time of writing.</p></li></ul>"})
# slides_create({"id": 18, "categoryId": 1, "topicId": 6, "slideNum": 2, "slide": "<br /><h1>Max level by expansion and closing remarks</h1><hr /><ul><li><p><b>Classic WoW:</b> 60</p></li><li><p><b>The Burning Crusade:</b> 70</p></li><li><p><b>Wrath of the Lich King:</b> 80</p></li><li><p><b>Cataclysm:</b> 85</p></li><li><p><b>Mists of Pandaria:</b> 90</p></li><li><p><b>Warlords of Draenor:</b> 100</p></li><li><p><b>Legion:</b> 110</p></li><li><p><b>Battle for Azeroth:</b> 120</p></li><li>With this, you should have learned everything you need to know to play and enjoy the game. If you find yourself bored within the game, we invite you to check our other categories, which present more content available in the game. But, before that, we need to test how much information you have retained. And that means quiz time!</p></li></ul>"})
# slides_create({"id": 19, "categoryId": 2, "topicId": 7, "slideNum": 1, "slide": "<br /><h1>Introduction to PvE</h1><hr /><ul><li><p>Now that you have gotten a taste of what WoW has to offer, you can start diving in into other content available. This category deals with PvE, Player vs Environment.</p></li><li><p>PvE, as the name suggests, pits the player against mobs in the game. While what you have done until now, can technically be considered PvE, in Wow this mostly refer to dungeons, which we will discuss in a later topic, and raids, touched upon in the End Game category.</p></li></ul>"})
# slides_create({"id": 20, "categoryId": 2, "topicId": 7, "slideNum": 2, "slide": "<br /><h1>Introduction to PvE</h1><hr /><ul><li><p>This type of content is usually taken on by groups of players with multiple tanks, healers and damage dealers, working together to take down the mobs that stand between yourself and the experience points, money and loot they guard.</p></li><li><p>Each of these instances have groups of tougher than usual mobs that you and your group must face. Besides those, you will also face a series of powerfull mobs, called bosses.</p></li><li><p>In the following topics, we will discuss the notions of PvE that you will need to know while leveling up, with the rest being presented in the End Game category. But for now, quiz time!</p></li></ul>"})
# slides_create({"id": 21, "categoryId": 2, "topicId": 8, "slideNum": 1, "slide": "<br /><h1>Combat Roles Introduction</h1><hr /><ul><li><p>In PvE, your role in the player group is clearly definded. You will either be a damage dealer, a tank or a healer. These designations are necessary for players to know what they must do while the group as a whole takes on the PvE instance.</p></li><li><p>Your combat role will be determined by the class specialization you chose at level 10. Some classes have access to all combat roles, like druids and paladins, but what is generally true is the fact that each class has a damage spec.</p></li><li><p>In the following slide, we will present all the damage roles and their responsibilities in the PvE group.</p></li></ul>"})
# slides_create({"id": 22, "categoryId": 2, "topicId": 8, "slideNum": 2, "slide": "<br /><h1>Combat Roles Description and closing remarks </h1><hr /><ul><li><p><b>1. DPS(damage dealer):</b> Your job is the most simple of them all. All you have to do is o follow the tanks lead, avoid AoE attacks and hit the boss to do the most damage possible in the shortest ammount of time possible. Aditionally, some bosses have certain mechanics that require you to interact with the environment. This is also the DPS's job, as the tank must keep the mobs busy and the healers must continue healing.</p></li><li><p><b>2. Tanks:</b> Your job is mainly to take hits from the boss and to keep it's attention on you. You do this by attacking it with your abillities, which most of the time, generate more threat than normal. You do not need to worry about other group memebers position, just yourself and the boss. Certain bosses require them to be standing in a certain spot or to be moving around to exploit their weekness or to stop them from using an ability. It is also your job to take care of that, as they will have their attention on you. Another aspect is the fact you may also have some healing abilities that you can cast on yourself. Consider using those when you are in a bind, as it eases the strain on the healer.</p></li><li><p><b>3. Healer:</b> Your job is, arguably, the most important of them all, and that is, keeping your group alive. Groups are made or broken by healers, as, besides managing your abilities to counteract the boss's damage, you also have to make up for whatever mistake the other members of the group are making. You have to stay as far away from the combat as possible, because, if the tank loses threat, it is possible for you to catch their attention.</p></li><li><p>These are the basic combat roles and their responsabilities. Of course, there are a lot more responsabilities to be taken by each member of the group, but that depends on the instance you're taking on. In the next topic, we will take a look at dungeons, one type of PvE instance, but before that, quiz time!</p></li></ul>"})
# slides_create({"id": 23, "categoryId": 2, "topicId": 9, "slideNum": 1, "slide": "<br /><h1>Dungeons</h1><hr /><ul><li><p>Dungeons are PvE instances that can be taken on in 5 man groups, one tank, one healer and 3 damage dealers. Their entrances are found in the world in numerous locations, for example Deadmines, which is located in Westfall. You can form a group of maximum 5 people with whatever combat role combination you want and travel to the entrance to take it on.</p></li><li><p>Alternativelly, you can use the Dungeon Finder, available from level 15 onwards, to search for a group and be teleported to the instance as soon as a group is formed. The disadvantage of that is that the party makeup will be a tank, a healer and 3 damage dealers, but that is the standard dungeon group makeup, anyway.</p></li><li><p>In the next slide we will discuss about how dungeons can hel speed up your leveling process.</p></li></ul>"})
# slides_create({"id": 24, "categoryId": 2, "topicId": 9, "slideNum": 2, "slide": "<br /><h1>Dungeons and the leveling process</h1><hr /><ul><li><p>Starting from level 15, you can start using the dungeon finder to help speed your journey to max level. The mai reason you want to do this is for the better gear and the experience.</p></li><li><p>Each dungeon has a series of quests or objectives which can be completed that grant a large ammount of experience points. Usually, if your level is on the lower scale of the dungeon level range, a dungeon run will usually result in a level or two just from the dungeon quests alone. This is, of course, not taking into account the experience you will get from killing the mobs in the dungeon that, as they are tougher than usual mobs, will grant you more experience and money.</p></li><li><p>As for the gear, each boss is guaranteed to drop a piece of rare equipment for your class and specialization, by which we mean that it will boost the stats you need for the specialization you currently have set. For example, if you are a Marksmanship Hunter when you defeat a boss, the gear you get will work best for a Marksmanship Hunter. There are also pieces of loot that will drop randomly and which will be shared between the members of your group. For those items you have to roll a number, and whoever gets the highest number, gets to keep the item.</p></li><li><p>For now, this is all you need to know about dungeons and PvE in general, as we will discuss more in the End Game category in the PvE topic. And for now, quiz time!</p></li></ul>"})
# slides_create({"id": 25, "categoryId": 3, "topicId": 10, "slideNum": 1, "slide":"<br /><h1>Introduction to PvP</h1><hr /><ul><li><p>Unlike PvE, in PvP, or player versus player you are fighting other players, as such you cannot rely on your game knowledge up until this point to help you To advance in PvP, you must play PvP matches against other players and learn what each class and spec can do.</p></li><li><p>There is also the fact that, again, unlike PvE, in PvP you won't mostly be using a predefined set of skills in a particular order. You must keep in mind that all other classes have an almost equal level of versatility as yours and that they always have an out of any situation you put them in, unless they used it already.</p></li><li><p>In this topic we will discuss a few general PvP notions and key concepts, in the hope that this will serve as your base in representing your faction on the battlefield.</p></li></ul>"})
# slides_create({"id": 26, "categoryId": 3, "topicId": 10, "slideNum": 2, "slide": "<br /><h1>PvP Concepts</h1><hr /><ul><li><p>First of all, who will you be fighting? Your opponents will be members of the opposite faction, which you choose when you created your character and selected your race. In the basics category, we mentioned that all races have a few unique bonuses. This is where they come in handy, as some of the races have abilities that can give them an edge in PvP. You'll have to learn what each race brings to the table to know what to expect in a match.</p></li><li><p>Another concept is to learn your own abilities. Unlike in PvE, you won't have the luxury of a chain of abilities that you can churn out in sequence. Some abilities have secondary effects that will be life saving in a match. For example, a Protection Paladin's Avenger's Shield can intrerupt every target it hits. In PvE, it is usually used as an intrerupt for a single target, however, since it can hit up to 3 targets that are close to each other and can stop the targets hit from using an abillity of the same type for a few seconds, it is better served in PvP to use it on a group of enemies clustered together.</p></li><li><p>Another example of such a skill is a Frost Mage's Frostbolt which can slow a target. In PvE, the slowing effect is almost negligeble, this skill being used more as a resource building spell, however, in PvP, it can also serve as a way to make your opponent take more time to reach you, as the slow effect takes a while to expire.</p></li><li><p>All in all, the secret of PvP combat is to know your class and what it is good against and your opponent's classes and races, to know how to react to whatever they can come up with. In the following topics, we will discuss some of the PvP content and how you can access them. And now, quiz time!</p></li></ul>"})
# slides_create({"id": 27, "categoryId": 3, "topicId": 11, "slideNum": 1, "slide": "<br /><h1>Arenas</h1><hr /><ul><li><p>Arenas are 2 versus 2 or 3 versus 3 matches in which you and your team will fight a team from the opposite faction in an empty space If these sound like gladiator matches, then good! It will help you get the right picture.</p></li><li><p>Arena matches can be rated or unrated, also known as skirmish mode, and are matched according to your previous arena performances. In other words, the more you win, the tougher opponents you will get. To win the match, you must eliminate the players from the other team. If 25 minutes pass and neither team is defeated, the match will end in a draw.</p></li><li><p>At the end of each match, you will be shown a window in which you will see the statistics of the match, and your updated MMR(Matchmaking rating), which is used to put you in balanced teams when searching for arena teams. You will also get Honor, which can be considered PvP experience points.</p></li></ul>"})
# slides_create({"id": 28, "categoryId": 3, "topicId": 11, "slideNum": 2, "slide": "<br /><h1>Skirmishes</h1><hr /><ul><li><p>Seeing as rated arena matches are locked at max level, we will touch upon unrated matches, or skirmishes. These are casual matches, in teams of 2 or 3 people, at the end of which you will get a number of Honor points, which are used to level your Honor rating, which will unlock some perks available only in PvP matches, and experience points.</p></li><li><p>You will be able to queue up for arena matches, in the same place as the Dungeon Finder, starting from level 15. The match making is based primarily on your level, as there are brackets for every 10 levels. Even so, there is a big difference between a level 15 and a level 25, for example, and, as such, your level will be raised to match the maximum level of the bracket you are in.</p></li><li><p>In this topic, we took a look at arena matches, what you need to do to get into one, what it is about and what you will get when you complete one. In the next topic, we will dive into battlegrounds, but, for now, quiz time!</p></li></ul>"})
# slides_create({"id": 29, "categoryId": 3, "topicId": 12, "slideNum": 1, "slide": "<br /><h1>Battlegrounds</h1><hr /><ul><li><p>While arena matches are a small scale fight to the death with 4 or six players, a round of battlegrounds is more akin to an operation, where the goal is not to kill opponents to win, though that certainly helps, but to complete a series of objectives. Most of these objectives vary between battlegrounds themselves, like how Alterac Valley is a Capture the Flag style battleground, while Arathi Basin is a Control Point battleground.</p></li><li><p>In terms of how similar they are to arenas, the way to get into a battlegrounds match and what you get after a battlegrounds match is the same as the arena matches. In fact, in the case of honor points, you can get significantly more from battlegrounds rather than arenas. However, this is where the similarities stop. As we said previously, to win a battlegrounds match, you have to complete a series of objectives which will give your team points. When a team reaches a certain number of points, they win, and, when the timer runs out, and neither team gathered enough points to win, the team with more points takes the match.</p></li><li><p>Like arena matches, battleground matches are rated, available only after you reach max level, and unrated, available from level 15 and split into brackets delimitated by level. Unlike arena matches, however, the team size is dictated by the battleground in which you fight in and can range from 10 to 40 players. As such, in a match, you will need a wide variety of players with different combat roles.</p></li><li><p>In the next slide, we will look at different types of battlegrounds rules and their win condition.</p></li></ul>"})
# slides_create({"id": 30, "categoryId": 3, "topicId": 12, "slideNum": 2, "slide": "<br /><h1>Types of battlegrounds rule sets</h1><hr /><ul><li><p><b>1) Resource Race: </b>Keep the resources flowing into the stores of your faction by capturing and holding nodes. Stall the enemy, disrupt their operations and you may soon be cruisin’ the road to victory.</p></li><li><p><b>2) Capture the Flag: </b>Sneak into the enemy’s fort, take their symbol of pride (flag) and return triumphantly (means: alive) to your own flag. Do not allow the enemy to take your flag or you will be unable to capture theirs.</p></li><li><p><b>3) Warfare: </b>Ride into the fields of battle and clash with the enemy force. Burn down their towers, obliterate their defences and eliminate, or secure, high priority targets.</p></li><li><p>All in all, these are the types of battlegrounds rule sets you will see while playing this type of content. This marks the end of the PvP category, in which we presented all the information you need to know about PvP until max level. We will discuss more about PvP in the End Game chapters. But, to see if you have absorbed all the information in this topic, quiz time!</p></li></ul>"})
# slides_create({"id": 31, "categoryId": 4, "topicId": 13, "slideNum": 1, "slide":"<br /><h1>Professions</h1><hr /><ul><li><p>A profession is a trade-oriented set of skills that player characters may learn and incrementally advance in order to gather, make, or enhance items that can be used in World of Warcraft gameplay. In essence, professions are 'jobs' characters may have. Professions are learned and improved via a trainer for a nominal fee, or sometimes advanced with special recipes. Any profession can be learned regardless of a character's faction, race, or class, although some racial traits provide bonuses to a particular profession.</p></li><li><p>Professions can be split down in 2 ways, first the primary and secondary and the second being gahering and production. In terms of primary or secondary professions, a character can only learn 2 primary professions, but can learn all the secondary ones. In the next slides, we will present you with a list of professions, what they produce and the type of that they are.</p></li></ul>"})
# slides_create({"id": 32, "categoryId": 4, "topicId": 13, "slideNum": 2, "slide": "<br /><h1>Gathering profession</h1><hr /><ul><li><p><b>1) Herbalism: </b>Harvest herbs found throughout the world and from the bodies of some creatures. You can detect nearby herbs on the minimap.<br><b>Type: </b>Primary</p></li><li><p><b>2) Skinning: </b>Skin the corpses of certain creatures for their hides, leather, and scales.<br><b>Type: </b>Primary</p></li><li><p><b>3) Mining: </b>Mine ore, stones, and raw gems from protruding veins or deposits. Also teaches you the smelting sub-profession, which allows the use of a forge to smelt the ore into bars of metal. You can detect nearby ore deposits on the minimap.<br><b>Type: </b>Primary</p></li><li><p><b>4) Fishing: </b>Fish from lakes, rivers, oceans and more using a  Fishing Pole. The items you fish up can be anything from gray junk to epic treasures and even rideable mounts!<br><b>Type: </b>Secondary</p></li><li><p><b>5) Archeology: </b>Unearth and reconstruct valuable artifacts from the cultures across Azeroth and beyond, often earning unique and mysterious rewards.<br><b>Type: </b>Secondary</p></li></ul>"})
# slides_create({"id": 33, "categoryId": 4, "topicId": 13, "slideNum": 3, "slide": "<br /><h1>Production profession</h1><hr /><ul><li><p><b>1) Alchemy: </b>	Mix potions, elixirs, flasks, oils and other alchemical substances into vials using herbs and other reagents. Your concoctions can restore health and mana, enhance attributes, or provide any number of other useful (or not-so-useful) effects<br><b>Type: </b>Primary</p></li><li><p><b>2) Blacksmithing: </b>Smith various melee weapons, mail and plate armor, and other useful trade goods like skeleton keys, shield-spikes and weapon chains to prevent disarming. Blacksmiths can also make various stones to provide temporary physical buffs to weapons.<br><b>Type: </b>Primary</p></li><li><p><b>3) Enchanting: </b>Imbue all manner of equipable items with magical properties and enhancements using dusts, essences and shards gained by disenchanting (breaking down) magical items that are no longer useful. Enchanters can also make a few low-level wands, as well as oils that can be applied to weapons providing a temporary magical buff.<br><b>Type: </b>Primary</p></li><li><p><b>4) Engineering: </b>Engineer a wide range of mechanical devices—including trinkets, guns, goggles, explosives and mechanical pets—using metal, minerals, and stone. As most engineering products can only be used by suitably adept engineers, it is not as profitable as the other professions; it is, however, often taken to be one of the most entertaining, affording its adherents with numerous unconventional and situationally useful abilities.<br><b>Type: </b>Primary</p></li><li><p><b>5) Jewelcrafting: </b>Cut and polish powerful gems that can be socketed into armor and weapons to augment their attributes or fashioned into rings, necklaces, trinkets, and jeweled headpieces. Also teaches you the  [Prospecting] ability, which sifts through raw ores to uncover the precious gems needed for your craft.<br><b>Type: </b>Primary</p></li><li><p><b>6) Leatherworking: </b>Work leather and hides into goods such as leather and mail armor, armor kits, and some capes. Leatherworkers can also produce a number of utility items including large profession bags, ability-augmenting drums, and riding crops to increase mount speed.<br><b>Type: </b>Primary</p></li><li><p><b>7) Tailoring: </b>Sew cloth armor and many kinds of bags using dye, thread and cloth gathered from humanoid enemies during your travels. Tailors can also fashion nets to slow enemies with, rideable flying carpets, and magical threads which empower items they are stitched into.<br><b>Type: </b>Primary</p></li><li><p><b>8) Cooking: </b>Combine ingredients into delicious food that can restore health and mana and provide temporary buffs. These buffs can increase most any attribute, and some give you unusual passive abilities. Also teaches you the ability to create campfires, which are usually required to cook.<br><b>Type: </b>Secondary</p></li></ul>"})
# slides_create({"id": 34, "categoryId": 4, "topicId": 14, "slideNum": 1, "slide": "<br /><h1>Reputation</h1><hr /><ul><li><p>Aside from the Alliance and the Horde, there are a lot of smaller factions in Azeroth.You can gain or lose favor, otherwise known as reputation, with many of the these factions. Higher reputation gives access to special rewards or new quests to accomplish.</p></li><li><p>To increase your reputation you can complete quests aligned with the faction, including repeatable reputation quests, kill certain enemies that the faction considers enemies and doing dungeons while wearing a faction's tabard.</p></li><li><p>Every playable race has a home faction, for example Darnassus for night elves, Orgrimmar for orcs, etc. Gaining reputation with your home faction provides access to discounted prices at their vendors, and certain reputation achievements. However, gaining reputation with other factions may provide additional benefits, such as specific items, patterns, spells etc which can be purchased from vendors of that faction at certain reputation levels.</p></li><li><p>In the next slide, we will take a look at the different reputation ranks and how many reputation points are needed for each.</p></li></ul>"})
# slides_create({"id": 35, "categoryId": 4, "topicId": 14, "slideNum": 2, "slide": "<br /><h1>Reputation points and ranks</h1><hr /><ul><li><p>Reputation is very similar to experience. Reputation is divided into a number of different levels for which players must earn reputation points to progress through. Higher reputation levels generally require more points than the previous level to progress. The exception is hated, which spans a very large number of reputation points. Reputation points, just like experience points, are gained through quests and killing various mobs. Unlike experience, it is possible to lose reputation points with some factions, either by killing members of the faction or by assisting rival factions. For rival factions, gaining reputation with one will normally decrease reputation with the other, but at a greater rate. The reputation ranks are as follows:</p></li><li><p><b>1) Hated:</b> 36000 points to next rank.</p></li><li><p><b>2) Hostile:</b> 3000 points to next rank.</p></li><li><p><b>3) Unfriendly:</b> 3000 points to next rank.</p></li><li><p><b>4) Neutral:</b> 3000 points to next rank.</p></li><li><p><b>4) Friendly:</b> 6000 points to next rank.</p></li><li><p><b>5) Honored:</b> 12000 points to next rank.</p></li><li><p><b>6) Revered:</b> 21000 points to next rank.</p></li><li><p><b>7) Exalted:</b> Maximum reputation rank.</p></li></ul>"})
# slides_create({"id": 36, "categoryId": 4, "topicId": 15, "slideNum": 1, "slide": "<br /><h1>Gold farming</h1><hr /><ul><li><p>Playing the game with the express intention of getting as much gold as possible is reffered to as gold farming. This is not to be confused with the players that farm gold with the express purpose of selling in-game money they accumulate with real-world money.</p></li><li><p>The gold farmers we are discussing are players that follow the game rules to get a big pile of in-game money for themselves, without the use of third party software, such as bots that kill creatures endlessly for the items they drop and selling said items to a vendor.</p></li><li><p>Gold is the primary in-game currency in World of Warcraft. You will need it to buy items from vendors and from other players on the Auction House. And seeing as the end game content necessitates a few consumables and services, such as potions, flasks, enchantments and gems, it is good to have a nice pile of gold saved up. And that's not to mention the quality of life improvments from items made by blacksmiths, leatherworkers and tailors. Even more so, some useful mounts, like the Traveller's Tundra Mammoth, which comes with a vendor and a repair NPC, which can be a literal lifesaver in endgame content, costs at the least 18000 gold.</p></li><li><p>In the next slide, we will go over some methods of generating large ammounts of gold and with which you will be able to build your fortune</p></li></ul>"})
# slides_create({"id": 37, "categoryId": 4, "topicId": 15, "slideNum": 2, "slide": "<br /><h1>Gold farming methods</h1><hr /><ul><li><p><b>1) Professions: </b>One of the most consistent way of farming gold is through professions. There is always a need of advanced potions, flasks, enchantments and gems, as these items can give you a considerable boost in combat. There is also the armor from leatherworkers, blacksmiths and tailors to consider, not to mention repair hammers from the engineers, that can be used by anyone. While this may seem as quite a lucrative venture, a better idea would be to pick up 2 gathering professions, as all the production professions need materials from at least 2 gathering ones. As such, it is highly improbable that they will have a way to get those materials without going to the Auction House. And this way, the only resource you will lose is time, which would be lost anyway, since to get started in a production profession needs a lot of gold, and that doesn't grow on trees.</p></li><li><p><b>2) Transmog farming: </b>Transmogs are a way to disguise your current armor with other pieces of armor you have worn on this character without losing the benefits of your armor set. Since the introduction of this system, there has been a resurgenec in price for some older pieces of armor from lower level dungeons that looked good. The strategy is this: search for a low level piece of equipment that can be farmed in a no-longer-current dungeon or raid that drops from trash mobs before the first unavoidable boss; go there and kill said thrash mobs; see if you have gotten the piece of armor in question; get out of the instance and reset it, then go in again and repeat. This strategy can also be used for whatever high-value item that can be dropped by a mob, however, it is not very consistent, as you are at the mercy of the random loot drop chance.</p></li><li><p><b>3) Kill thrash mobs in a dungeon instance: </b>While more consistent than transmog farming, but more tedious and less rewarding than both of the previous methods, this strategy involves going to an instance, defeating all the trash mobs possible without killing the boss, looting everything and selling it to a vendor. If you choose to do this, we recommend to buy the Traveller's Tundra Mammoth, because going back to a vendor everytime your bag gets full is quite a large ammount of time wasted.</p></li><li><p>These gold farming methods have been tried and tested by many players along the years, including yours truly. This marks the end of the Gold Farming topic, and of the Farming category itself. As such, we will give you a quiz to make sure you got all the information needed from these lessons.</p></li></ul>"})
# slides_create({"id": 38, "categoryId": 5, "topicId": 16, "slideNum": 1, "slide": "<br /><h1>End Game introduction</h1><hr /><ul><li><p>So, you have reached max level and think that this is it? That you have no way to progress any further, that you are a part of this game's elite few at the maximum level? Well, think again, because the game's just started!</p></li><li><p>Up until this point, you have had a pretty easy time in the game, however, this changes at max level. From the PvE standpoint, you're now able to do harder versions of dungeons than before and can finally start raiding after a few dungeon runs.</p></li><li><p>While from a PvP standpoint, you can finally participate in ranked matches and represent your faction in the ongoing battle between the Alliance and Horde, gaining prestige and titles for your deeds(at the end of the PvP seasons)! And as for farming, well, not much has really changed. About the only good things that comes from max level in the farming department is the fact that now you can meet all the factions possible that have reputation systems, can access all the instances that you can complete alone, and have access to current recipes for the production professions.</p></li></ul>"})
# slides_create({"id": 39, "categoryId": 5, "topicId": 16, "slideNum": 2, "slide": "<br /><h1>End Game introduction</h1><hr /><ul><li><p>First, you'll need to know how to progress now that you're at the level cap. This is done through raising your average gear score, or as it's more commonly called, item level. If you look at your character screen, in the upper left portion, you will see a big number; that is your item level. To find an items gear score, hover your cursor over the item and it's stats will pop up. Under the items name, you will see that item's gear score.</p></li><li><p>If you have some quests left unfinished in the highest level zone, now's the time to do them, as most of the quests will give you a piece of gear that most likely is better than what you have already. Depending on the expansion you are currently playing in, there might be some daily quests or objectives which can reward you with equipment. One side benefit of doing this is the fact that the game will now reward you with additional gold instead of experience, meaning that you will also gather some money for consumables down the line if you decide to get into PvP or PvE.</p></li><li><p>After getting some good gear, you'll have to make sure you know how to play your class and spec. Even though you fell like you're playing it to the best of ot's capabilities, remember that there are more experienced players than you who have perfected their class and spec combat style to the most it can be allowed by the game. You can find a guide on how to play your style on the internet easily enough.</p></li><li><p>And now, after all the preparation, you can finally get into PvP or PvE. But first, quiz time!</p></li></ul>"})
# slides_create({"id": 40, "categoryId": 5, "topicId": 17, "slideNum": 1, "slide": "<br /><h1>End Game PvE</h1><hr /><ul><li><p>Now that you have some better gear, you can start oing PvE content. If you open your dungeon finder, you can find a new section has appeared, called Heroic Dungeons. These dungeons are more difficult than regular dungeons, but reward you with better gear. This is the first step you have to take to start, as what's to come will be even more challenging, even with the loot from the heroic dungeons.</p></li><li><p>After this comes a new difficulty setting, and it is called mythic. Mythic dungeons are even more difficult than heroic, with the enemies having more health and doing more damage to you. Some may even have an additional ability, that is seen only from here and up on the dificulty scale. Also, starting at this dungeon difficulty, you'll need to create your own group of players, but the upside is that it can have whatever composition of combat roles you want. As expected, the gear you can obtain from mythic dungeons is better than the loot from the heroic dungeons. After completing your first mythic dungeon, you will also receive the key(literally), to the next level of dungeon difficulty, a Mythic Keystone, allowing you to do Mythic+ dungeons.</p></li><li><p>Mythic+ dungeons are some of the hardest pieces of PvE content you can do. First of all, unlike the other difficulty of dungeons, you have a time limit, the ammount depending on the dungeon. Also, in addition to the bosses, you have to defeat a certain number of trash mobs to succesfully complete the instance. The difficulty depende on the level of the key you used at the entrance. The higher the level, the more difficult it will be.</p></li><li><p>Starting at key level 4, the keystone will also have some additional affixes which can make the dungeon more difficult. Some examples of these effects are trash mobs being buffed when another mobs dies near it, bosses doing 35% more damage than normal and having to kill more mobs to fill the percentage bar, and thus, causing more mobs to spawn. Each 5 key levels, or there about, the key will pick up an additional affix, with 3 affixes being the max. Fortunatly, the affixes are the same for every key during the a singel week, and you can look and see what they are even before getting a key.</p></li><li><p>Also, if the holder of the key leaves the instance, it will be considered a failed run. Other members can leave, but you cannot replace someone if the dungeon has already started. If the timer reaches 0, you can still continue the run and suffer no consequences, however, you will also not recieve as much loot. The penalty of failing a run is the demotion of the key with one level, while if you succeed, the key will be replaced with another one that is one or two levels higher, depending on your time, in another dungeon.</p></li><li><p>Doing Mythic+ dungeons is guaranteed to get you some gear with a prety high item level, however, if you truly want the best equipment, you have to do the pinnacle of PvE content, Raiding.</p></li></ul>"})
# slides_create({"id": 41, "categoryId": 5, "topicId": 17, "slideNum": 2, "slide": "<br /><h1>Raiding</h1><hr /><ul><li><p>If you want the best gear, the only way it can be obtained is through raids. While, teoretically, Mythic+ dungeons can give you items with a higher item level than most of the easier difficulty raids, in raids you can get set armor, which gives you bonus perks for wearing multiple pieces and wearing them all gives you the most powerful bonus.</p></li><li><p>Raids are done in groups of at least 10 to a maximum of 30 players, with no set ideal makeup of combat roles. Some raids require 2 tanks, some require 7 healers, etc. The difficulty of the raid is based mostly on the mechanics, however in terms of mobs' health and damage, it scales depending on the size of the group you are in.</p></li><li><p>There are 4 difficulty settings for raids, LFR(Raid Finder), Normal, Heroic and Mythic. LFR, or Raid Finder, is the easiest and experienced by most players, as this is the only category which can be joined with the Raid Finder. After LFR, we have Normal and Heroic, which require some pretty high level gear and skill to do, but they are not overly difficult if you know what you are doing and have some good gear.</p></li><li><p>Last but not least, we have Mythic Raiding, something only about 5% of the player base can say they have acomplished, and even that is a positive estimate. Mythic Raiding is the hardest piece of PvE content available, someone needing a full set of the maximum item level gear from previous raid difficulty, have the best consumables for their class and spec and to know their class better than the back of their hand.</p></li><li><p>It is so difficult that Blizzard awards the first group to finish each boss of a mythic raid a special achievment, World's First, and for good reason. To give you a better idea of it's difficulty, there have been bosses that have never been defeated even after a month since it's release. And we're not talking about the whole raid, only one boss. However, once a guild figures out a tactic to defeat each boss of the raid, more and more players will complete it, at least those that have the gear and skills to be mythic raiders.</p></li><li><p>A new raid, with all it's difficulty settings, is released in every patch of an expansion, and the loot from the Mythic Raid is the best loot of said patch. And with that, we have finished presenting you with some general concepts of the end game from a PvE standpoint. In the next topic, we will take a look at it from a PvP perspective, but for now, quiz time!</p></li></ul>"})
# slides_create({"id": 42, "categoryId": 5, "topicId": 18, "slideNum": 1, "slide": "<br /><h1>End Game PvP</h1><hr /><ul><li><p>Maybe you don't want to do PvE. Maybe you want to fight actual players instead of superpowered mobs. In that case, first thing you'll want to do is to max out your honor talents and get some PvP gear, as even a decent piece of PvP gear is more useful than one from a Mythic Raid in a PvP match. Depending on the expansion, you will have to do casual battlegrounds and arena matches or do some of the daily quests and objectives related to PvP. However, be careful, as after you deactivate PvP, it will take 5 minutes for it to completely wear off unless you enter a neutral Rest Zone, like Dalaran.</p></li><li><p>After maxing out your honor rating and getting all the perks and some decent gear, you can then get right into rated matches. For now, we will discuss rated battlegrounds and in the next slide, we will take a look at rated arena matches.</p></li><li><p>Unlike in casual battlegrounds matches, the only objective that should be in yours and your team's minds is completing the objective and winning the match. The best strategy is making sure you know where your team is and where the enemy team is. One of the best advantages you can have is good comunication and coordonation with your teammates.</p></li><li><p>That, however, is only a basic strategy that will work in almost every situation when you start doing rated battlegrounds, but soon, as you win more and more matches, you will need to adapt to every battleground and it's rule set. As for rewards, winning a rated battleground gets you Conquest Points. Earn enought of them and, at the end of the PvP season, you will get a title and an exclusive ground mount for this PvP season.</p></li><li><p>In the next slide, we will take a look at the rated arena matches.</p></li></ul>"})
# slides_create({"id": 43, "categoryId": 5, "topicId": 18, "slideNum": 2, "slide": "<br /><h1>Rated Arena Matches</h1><hr /><ul><li><p>While rated battlegroundsmatches will get you some practice and tons of rewards, this is where your skill will be tested, as tipically, you won't have the numbers advantage and once you are defeated, you can't simply respawn, only wait for the match to be over.</p></li><li><p>In skirmishes, if you lose, you only have to worry about your MMR, matchmaking rating, which is based off of the team performance. However, now there is also a Personal Rating, based only on your performance. This means that, even if your team wins, but you didn't do much, you won't stand to gain many points. On the other hand, however, if you lose, but you did an impressive job against the other team, you won't lose as much rating as usual.</p></li><li><p>In rated matches, you will be limited in regards to consumables and abilities you can use. For example, you can't use ressurection spells and flasks, to give a few examples, but you can use conjured refreshments, if you or one of your team members is a mage, bandages and water purchased from a vendor.</p></li><li><p>If you win enough matches and get a personal rating of at least 2000, you will start being able to receive the elite equipment of the current season, the best PvP gear for the patch you are currently in, as a PvP season usualy starts with the release of a new patch. However, be warned that you will not receive the set all at once, but have a chance to get a piece of it after every match while, again, your personal rating is above 2000.</p></li><li><p>Like in battlegrounds matches, the key to success is having good communication and coordination with your teammates. Unlike in a battlegrounds match, however, your teammates class and spec must complement your own and viceversa. For example, 2 damage dealers are like a glass cannon, dealing a lot of damage, but easily defeated with a few good hits. A tank and a healer, on the other hand, have amazing survivability, but a low damage output. All in all, to use a tired cliche, it's all about teamwork and sinergy.</p></li><li><p>And that is all for this topic, and this category. While PvP may not be as glamorous as PvE, it is still challenging, and succeding can get you the same rush as a caompleted raid. And now, to see if you have absorbed all the knowledge in this topic, quiz time!</p></li></ul>"})

# Quiz
# quiz_create({"id": 1, "categoryId": 1, "topicId": 1, "question": "What do you see after you first log into World of Warcraft?", "answer_A": "A map of the world", "answer_B": "The character creation screen", "answer_C": "A cinematic", "correctAnswer": "B"})
# quiz_create({"id": 2, "categoryId": 1, "topicId": 1, "question": "What is on the right side of the character customization screen?", "answer_A": "A list of body part options", "answer_B": "The race selection list", "answer_C": "A list of customization option for the body part selected", "correctAnswer": "C"})
# quiz_create({"id": 3, "categoryId": 1, "topicId": 1, "question": "What can you use to create your character name?", "answer_A": "Anything, as long as the name is unique", "answer_B": "Only letters", "answer_C": "Letters, numbers and spaces", "correctAnswer": "B"})
# quiz_create({"id": 4, "categoryId": 1, "topicId": 2, "question": "Which of these cities is the capital of the Humans?", "answer_A": "Ironforge", "answer_B": "Gilneas", "answer_C": "Stormwind", "correctAnswer": "C"})
# quiz_create({"id": 5, "categoryId": 1, "topicId": 2, "question": "Which of these classes can an Orc be?", "answer_A": "Shaman", "answer_B": "Paladin", "answer_C": "Demon Hunter", "correctAnswer": "A"})
# quiz_create({"id": 6, "categoryId": 1, "topicId": 2, "question": "What faction are the Pandarens part of", "answer_A": "Alliance", "answer_B": "Horde", "answer_C": "They can choose upon completing the starter zone", "correctAnswer": "C"})
# quiz_create({"id": 7, "categoryId": 1, "topicId": 3, "question": "How many specs do Druids have?", "answer_A": "2", "answer_B": "3", "answer_C": "4", "correctAnswer": "C"})
# quiz_create({"id": 8, "categoryId": 1, "topicId": 3, "question": "Which of the following classes have access to specs that cover all 3 combar roles?", "answer_A": "Death Knight", "answer_B": "Priest", "answer_C": "Paladin", "correctAnswer": "C"})
# quiz_create({"id": 9, "categoryId": 1, "topicId": 3, "question": "At which level do Demon Hunters start?", "answer_A": "100", "answer_B": "98", "answer_C": "55", "correctAnswer": "B"})
# quiz_create({"id": 10, "categoryId": 1, "topicId": 4, "question": "What is a quest?", "answer_A": "A task given by an NPC", "answer_B": "Your personal goal in this game", "answer_C": "Your factions goal for the week", "correctAnswer": "A"})
# quiz_create({"id": 11, "categoryId": 1, "topicId": 4, "question": "What is a quest chain?", "answer_A": "A chain involved in a quest", "answer_B": "A quest with multiple objectives", "answer_C": "A series of quests", "correctAnswer": "C"})
# quiz_create({"id": 12, "categoryId": 1, "topicId": 4, "question": "What is a quest hub?", "answer_A": "A location related to a quest", "answer_B": "A place where you can find multiple quests", "answer_C": "A place which is a target of a quest.", "correctAnswer": "B"})
# quiz_create({"id": 13, "categoryId": 1, "topicId": 5, "question": "Select the combat modifiers", "answer_A": "Stunned", "answer_B": "Rooted", "answer_C": "Both", "correctAnswer": "C"})
# quiz_create({"id": 14, "categoryId": 1, "topicId": 5, "question": "Damage over Time on a target indicates", "answer_A": "The target is taking damage periodically over a set ammount of time", "answer_B": "The target is dealing damage to someone else since it entered combat", "answer_C": "The damage the target has taken per second since it entered combat", "correctAnswer": "A"})
# quiz_create({"id": 15, "categoryId": 1, "topicId": 5, "question": "What is the concept of active cooldown?", "answer_A": "The time for the debuff on taking fire damage to wear off.", "answer_B": "The time it takes for a character to use another skill", "answer_C": "The time it takes to use the same skill again", "correctAnswer": "C"})
# quiz_create({"id": 16, "categoryId": 1, "topicId": 6, "question": "What is the maximum level in the Wrath of the Lich King expansion?", "answer_A": "70", "answer_B": "120", "answer_C": "80", "correctAnswer": "C"})
# quiz_create({"id": 17, "categoryId": 1, "topicId": 6, "question": "When does your character level up?", "answer_A": "When you get enough experience points", "answer_B": "When you complete a certain quest", "answer_C": "When you defeat a certain enemy", "correctAnswer": "A"})
# quiz_create({"id": 18, "categoryId": 1, "topicId": 6, "question": "When should you leave a zone?", "answer_A": "When you get to max level", "answer_B": "When you get to the max level range of the zone", "answer_C": "When you complete the zone story line", "correctAnswer": "B"})
# quiz_create({"id": 19, "categoryId": 2, "topicId": 7, "question": "What does PvE refer to?", "answer_A": "Raids and dungeons", "answer_B": "Doing quests", "answer_C": "Fighting against other players", "correctAnswer": "A"})
# quiz_create({"id": 20, "categoryId": 2, "topicId": 7, "question": "What do you get from doing PvE instances?", "answer_A": "Equipment", "answer_B": "Experience", "answer_C": "Both", "correctAnswer": "C"})
# quiz_create({"id": 21, "categoryId": 2, "topicId": 7, "question": "What do you mostly face in PvE instances?", "answer_A": "Logic puzzles", "answer_B": "Mobs that are more powerfull than usual", "answer_C": "Other players", "correctAnswer": "B"})
# quiz_create({"id": 22, "categoryId": 2, "topicId": 8, "question": "What is a tank's job?", "answer_A": "Take hits from the boss and lead it around", "answer_B": "Deal damage to the boss", "answer_C": "Heal the other group members", "correctAnswer": "A"})
# quiz_create({"id": 23, "categoryId": 2, "topicId": 8, "question": "What is a DPS's job?", "answer_A": "Take hits from the boss and lead it around", "answer_B": "Deal damage to the boss", "answer_C": "Heal the other group members", "correctAnswer": "B"})
# quiz_create({"id": 24, "categoryId": 2, "topicId": 8, "question": "What is a healer's job?", "answer_A": "Take hits from the boss and lead it around", "answer_B": "Deal damage to the boss", "answer_C": "Heal the other group members", "correctAnswer": "C"})
# quiz_create({"id": 25, "categoryId": 2, "topicId": 9, "question": "What is a dungeon?", "answer_A": "A PvE instance for 7 man groups", "answer_B": "A PvE instance for groups of maximum 10 men", "answer_C": "A PvE instance for 40 man groups", "correctAnswer": "B"})
# quiz_create({"id": 26, "categoryId": 2, "topicId": 9, "question": "What is a the ideal group composition for a dungeon group?", "answer_A": "1 Tank, 1 Healer, 3 DPS", "answer_B": "All DPS for maximum damage", "answer_C": "An equal split of tanks, healers and DPS, preferably 3 of each", "correctAnswer": "A"})
# quiz_create({"id": 27, "categoryId": 2, "topicId": 9, "question": "At what level can you start using the dungeon finder?", "answer_A": "Level 10", "answer_B": "Level 15", "answer_C": "Level Max", "correctAnswer": "B"})
# quiz_create({"id": 28, "categoryId": 3, "topicId": 10, "question": "What is PvP?", "answer_A": "Player versus Environment", "answer_B": "Player versus Player", "answer_C": "A rare epic mount", "correctAnswer": "B"})
# quiz_create({"id": 29, "categoryId": 3, "topicId": 10, "question": "Who do you fight against in PvP matches?", "answer_A": "Players from the opposite faction", "answer_B": "Players from your own faction", "answer_C": "Both", "correctAnswer": "A"})
# quiz_create({"id": 30, "categoryId": 3, "topicId": 10, "question": "What do you need to learn to be successful in PvP?", "answer_A": "Your own abilities", "answer_B": "Your opponent's abilities", "answer_C": "Both", "correctAnswer": "C"})
# quiz_create({"id": 31, "categoryId": 3, "topicId": 11, "question": "How many players are in an arena team?", "answer_A": "2", "answer_B": "3", "answer_C": "Either of the above", "correctAnswer": "C"})
# quiz_create({"id": 32, "categoryId": 3, "topicId": 11, "question": "What are skirmishes?", "answer_A": "An unrated arena match", "answer_B": "A rated arena match", "answer_C": "A battle between 2 players of the opposite faction that met in the world", "correctAnswer": "A"})
# quiz_create({"id": 33, "categoryId": 3, "topicId": 11, "question": "What are honor points?", "answer_A": "A players faction score in PvP instances", "answer_B": "PvP experience points", "answer_C": "Both", "correctAnswer": "B"})
# quiz_create({"id": 34, "categoryId": 3, "topicId": 12, "question": "What is a battlegrounds match?", "answer_A": "An arena match in a bigger arena", "answer_B": "A fight between 2 teams in a race to complete as many objectives as possible", "answer_C": "Both", "correctAnswer": "B"})
# quiz_create({"id": 35, "categoryId": 3, "topicId": 12, "question": "What do you get for completing a battlegrounds match?", "answer_A": "Honor points and experience", "answer_B": "Equipment for PvE scenarios", "answer_C": "Both", "correctAnswer": "A"})
# quiz_create({"id": 36, "categoryId": 3, "topicId": 12, "question": "What is the objective of a Resource Race match?", "answer_A": "Invade the enemy base and take something from it", "answer_B": "Disrupt the enemy territory and destroy as many of their resources as possible", "answer_C": "Capture and hold positions that grant your team points", "correctAnswer": "C"})
# quiz_create({"id": 37, "categoryId": 4, "topicId": 13, "question": "What is the objective of a profession?", "answer_A": "A way for players to produce items", "answer_B": "It is a role play term", "answer_C": "It is another term for a character specialization", "correctAnswer": "A"})
# quiz_create({"id": 38, "categoryId": 4, "topicId": 13, "question": "What type of profession is Leatherworkin?", "answer_A": "Secondary", "answer_B": "Gathering", "answer_C": "Production", "correctAnswer": "C"})
# quiz_create({"id": 39, "categoryId": 4, "topicId": 13, "question": "What type of profession is Cooking?", "answer_A": "Primary", "answer_B": "Secondary", "answer_C": "Gathering", "correctAnswer": "B"})
# quiz_create({"id": 40, "categoryId": 4, "topicId": 14, "question": "How many reputation points do you need to get to Hostile rank from Hated rank?", "answer_A": "21000", "answer_B": "36000", "answer_C": "You can't, if you are hated by a faction, you can't raise your reputation with it", "correctAnswer": "B"})
# quiz_create({"id": 41, "categoryId": 4, "topicId": 14, "question": "How many reputation points do you need to get to Neutral rank from Honored rank?", "answer_A": "9000", "answer_B": "21000", "answer_C": "15000", "correctAnswer": "A"})
# quiz_create({"id": 42, "categoryId": 4, "topicId": 14, "question": "How many reputation points are needed to raise your reputation from Exalted to the next rank?", "answer_A": "36000", "answer_B": "27000", "answer_C": "You can't, Exalted is the maximum reputation rank", "correctAnswer": "C"})
# quiz_create({"id": 43, "categoryId": 4, "topicId": 15, "question": "What is gold used for in World of Warcraft?", "answer_A": "Buying items from NPC vendors", "answer_B": "Buying items from other players through the Auction House", "answer_C": "Both", "correctAnswer": "C"})
# quiz_create({"id": 44, "categoryId": 4, "topicId": 15, "question": "Which is the most consistent method of farming gold?", "answer_A": "Selling items from professions", "answer_B": "Farming Transmogs", "answer_C": "Killing mobs and vendor-ing their loot", "correctAnswer": "A"})
# quiz_create({"id": 45, "categoryId": 4, "topicId": 15, "question": "Which is the most tedious method of farming gold?", "answer_A": "Selling items from professions", "answer_B": "Farming Transmogs", "answer_C": "Killing mobs and vendor-ing their loot", "correctAnswer": "C"})
# quiz_create({"id": 46, "categoryId": 5, "topicId": 16, "question": "What does it mean that you are at the level cap?", "answer_A": "That I'm part of the elite few that got to max level", "answer_B": "That I can't progress any further", "answer_C": "That the game has just started", "correctAnswer": "C"})
# quiz_create({"id": 47, "categoryId": 5, "topicId": 16, "question": "What should you first do at max level?", "answer_A": "Get some better gear from unfinished quests", "answer_B": "Start doing end game PvE instances", "answer_C": "Gather some gold to buy equipment", "correctAnswer": "A"})
# quiz_create({"id": 48, "categoryId": 5, "topicId": 16, "question": "And the second thing?", "answer_A": "PvP content", "answer_B": "Look up a guide on how to best play my class and specialization", "answer_C": "Start farming to buy some consumables", "correctAnswer": "B"})
# quiz_create({"id": 49, "categoryId": 5, "topicId": 17, "question": "When do you get a Mythic Keystone?", "answer_A": "After completing a heroic dungeon", "answer_B": "After completing a mythic dungeon", "answer_C": "As soon as you hit a certain item level", "correctAnswer": "B"})
# quiz_create({"id": 50, "categoryId": 5, "topicId": 17, "question": "What is the easiest difficulty of raiding?", "answer_A": "Normal", "answer_B": "Heroic", "answer_C": "LFR", "correctAnswer": "C"})
# quiz_create({"id": 51, "categoryId": 5, "topicId": 17, "question": "How hard are Mythic Raids?", "answer_A": "Pretty easy if you have good gear", "answer_B": "Alright if you have good gear and decen knowledge of your class and spec", "answer_C": "Extremely difficult, even if you have the best gear available beside the Mythic Raid and have mastered your class", "correctAnswer": "C"})
# quiz_create({"id": 52, "categoryId": 5, "topicId": 18, "question": "What should you do at max level in terms of PvP?", "answer_A": "Get som decent PvP gear", "answer_B": "Get all the Honor perks", "answer_C": "Both", "correctAnswer": "C"})
# quiz_create({"id": 53, "categoryId": 5, "topicId": 18, "question": "What is your goal in a rated battlegrounds match?", "answer_A": "Getting as many kills as possible kills to improve my K/D ration", "answer_B": "Winning by completing my objectives", "answer_C": "Disrupting my teammates that are doing better than me", "correctAnswer": "B"})
# quiz_create({"id": 54, "categoryId": 5, "topicId": 18, "question": "What is the most important factor in winning a rated arena match?", "answer_A": "Gear", "answer_B": "Doing the most damge to the enemy team", "answer_C": "Teamwork and synergy between teammate", "correctAnswer": "C"})


# Badges
# execute_command('DELETE FROM badges;')
# add_badge('Customize Yourself')
# add_badge('Orcs, Elfs and dwarfs')
# add_badge('Choose your weapon')
# add_badge('The task is at hand')
# add_badge('Time to fight')
# add_badge('What doesnt kill you...')
# add_badge('Prepare for the world')
# add_badge('We need a battle strategy')
# add_badge('Head into the darkness')
# add_badge('What you need to know')
# add_badge('The Arena awaits you')
# add_badge('Kill or be killed')
# add_badge('Get a Job')
# add_badge('Respect +')
# add_badge('Get that $')
# add_badge('Is this the end?')
# add_badge('Fight the monsters')
# add_badge('Fight the users')

print(loggedUsers_GetId("d65ce05938cdce781c3ae48f0c3c8adb"))



# print_tables()
print_table("users")
print("\n")
print_table("auth_table")
# ('authTable', print_table('auth_table'))
print_table("user_access_level")
print('\n')
# print_table('badges')
print('\n')
# print_table('quizzes')
# print_tables()
print('\n')
# print_table('users')
print('\n')
# print_table('badges')
print('\n')
# print('\n')
# print_table('quiz')
# print('\n')
# # print_table('badges')
# print('\n')
# print_table('quizzes')
# create_quiz()
# print('\n')
# print_table("users")
# create_topics()
# create_slides()
# print_table("logged_users")
# create_quiz_score()
# create_access()


